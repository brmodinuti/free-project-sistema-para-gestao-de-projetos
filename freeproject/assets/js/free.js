//Ações botões menu
$('#sair').click(function(){
	console.log('Sair');
});
$('#configuracao').click(function(){
	console.log('Configuração');
});
//Placeholder dos selects
$('select.placeholder').change(function(){
	if ($(this).children('option:first-child').is(':selected')) {
		$(this).addClass('placeholder');
	} else {
		$(this).removeClass('placeholder');
	}
});
/* SUPER MENU INICIO */
var clickSuperMenu = 0;
$("#super-botao").click(
	function(){
		clickSuperMenu++;
		if(clickSuperMenu % 2 != 0){
			$('.super-menu').removeClass('hidden');
			$('.super-menu>ul').addClass('animated');
		}else{
			$('.super-menu').addClass('hidden');
		}
	});
/* SUPER MENU FIM */
/* INPUTS DATA INICIO*/
$(function() {
	$(".data").datepicker({
		format: "dd/mm/yyyy",
		language: "pt-BR",
		autoclose: true,
		todayHighlight: true
	});
 });
/* INPUTS DATA FIM*/
$(document).ready(function(){
	$('.maskTel').mask('(99) 9999-9999');
	$('.maskCel').mask('(99) 99999-9999');
	$('.maskCEP').mask('99999-999');
	$('.maskDATA').mask('99/99/9999');
	$('.maskCPF').mask('999.999.999-99');
	$('.maskCNPJ').mask('99.999.999/9999-99');
	$("input[name='valor[]']").change();
	// $("select[name='tipopessoa']").change();
	// $("select[name='cidade']").change();
	// $("select[name='estado']").change();
	// $("select[name='cliente']").change();
	$("select").change();
})
/* INICIO VERIFICAÇÃO DOS CAMPOS */
function alert_message (opcao){
	/*
		0: ERRO
		1: SUCESSO
	*/
	if(opcao == 0){
		var html = 
			'<div class="alert alert-danger animated bounce" role="alert">'+
				'<strong>Alerta!</strong> Não foi possível realizar esta operação, preencha os campos destacados.'+
			'</div>';
		$('.mensagens').prepend(html);
		setTimeout(
			function(){
				$('.alert').removeClass('bounce');
				$('.alert').addClass('bounceOut');
			},5000
		);
		setTimeout(
			function(){
				$('.alert').remove();
			},5800
		);
	}
	if(opcao == 1){
		var html = 
			'<div class="alert alert-success animated bounce" role="alert">'+
				'<strong>Muito bom!</strong> Operação realizada com sucesso.'+
			'</div>';
		$('.mensagens').prepend(html);
		setTimeout(
			function(){
				$('.alert').removeClass('bounce');
				$('.alert').addClass('bounceOut');
			},5000
		);
		setTimeout(
			function(){
				$('.alert').remove();
			},5800
		);
	}
	if(opcao == 2){
		var html = 
			'<div class="alert alert-danger animated bounce" role="alert">'+
				'<strong>Alerta!</strong> CPF inválido.'+
			'</div>';
		$('.mensagens').prepend(html);
		setTimeout(
			function(){
				$('.alert').removeClass('bounce');
				$('.alert').addClass('bounceOut');
			},5000
		);
		setTimeout(
			function(){
				$('.alert').remove();
			},5800
		);
	}
	if(opcao == 3){
		var html = 
			'<div class="alert alert-danger animated bounce" role="alert">'+
				'<strong>Alerta!</strong> CNPJ inválido.'+
			'</div>';
		$('.mensagens').prepend(html);
		setTimeout(
			function(){
				$('.alert').removeClass('bounce');
				$('.alert').addClass('bounceOut');
			},5000
		);
		setTimeout(
			function(){
				$('.alert').remove();
			},5800
		);
	}
	if(opcao == 4){
		var html = 
			'<div class="alert alert-danger animated bounce" role="alert">'+
				'<strong>Alerta!</strong> Verifique campo destacado.'+
			'</div>';
		$('.mensagens').prepend(html);
		setTimeout(
			function(){
				$('.alert').removeClass('bounce');
				$('.alert').addClass('bounceOut');
			},5000
		);
		setTimeout(
			function(){
				$('.alert').remove();
			},5800
		);
	}
}
$("button[name='123456789']").click(function(){
	var x = 0;
	$('.req').each(function(i,element){
		element = $(element);
		var expressao = element.is(":visible") && element.val() == "";
		element.toggleClass('required',expressao);
		if(expressao == true){
			x++;
		}	
	});
	if(x>0){
		alert_message(0);
	}else{
		alert_message(1);
	}
});
/* FIM VERIFICAÇÃO DOS CAMPOS */

$("button[name='cliente-novo']").click(function(){
	window.location.replace('clientes/novo');
});

$("button[name='orcamento-novo']").click(function(){
	window.location.replace('orcamentos/novo');
});
$("button[name='projeto-novo']").click(function(){
	window.location.replace('orcamentos/novo');
});
/* INICIO MOSTRA INPUTS PF OU PJ */
$("select[name='tipopessoa']").change(
	function(){
		var tppessoa = $("select[name='tipopessoa']").val();
		if(tppessoa == 'PF'){
			$(".pj").addClass("hidden");
			$(".pf").removeClass("hidden");
		}else{
			$(".pj").removeClass("hidden");
			$(".pf").addClass("hidden");
		}
	}
);
/* FIM MOSTRA INPUTS PF OU PJ */
/* INICIO MOSTRAR SELECT EQUIPE */
$("select[name='tipousuario']").change(function(){
	if(this.value > 1 ){
		$('.equipe').removeClass('hidden');
		$('.email').removeClass('col-sm-9');
		$('.email').addClass('col-sm-6');
	}else{
		$('.equipe').addClass('hidden');
		$('.email').removeClass('col-sm-6');
		$('.email').addClass('col-sm-9');
	}
});
/* FIM MOSTRAR SELECT EQUIPE*/
/* 
$("select[name='estado']").change(function(){
	var arr = {
		'AC': [],
		'AL': [],
		'AP': [],
		'AM': [],
		'BA': [],
		'CE': [],
		'DF': [],
		'ES': [],
		'GO': [],
		'MA': [],
		'MT': [],
		'MS': [],
		'MG': [],
		'PA': [],
		'PB': [],
		'PR': ['Londrina','Cambé'],
		'PE': [],
		'PI': [],
		'RJ': [],
		'RN': [],
		'RS': [],
		'RO': [],
		'RR': [],
		'SC': [],
		'SP': ['Campinas','São Paulo'],
		'SE': [],
		'TO': []
	};
	var cidade = $("select[name=cidade]");
	cidade.children().not(":first").remove();
	arr[this.value].forEach(function(item){
		cidade.append($("<option>").text(item));
	});
});
 */

/* orçamento view */
function calcTotalLiquido(){
	var totalbruto = 0;
	$("input[name='subtotal']").each(function() {
		totalbruto = totalbruto + parseFloat($(this).val());
	});
	$("input[name='totalbruto']").val(totalbruto.toFixed(2));
	var desconto = parseFloat($("input[name='in-desconto']").val());
	if(totalbruto >= desconto){
		// alert('aa');
		$("input[name='totalliquido'],input[name='financeiro']").val((totalbruto-desconto).toFixed(2));
	}else{
		// alert('aa');
		$("input[name='totalliquido'],input[name='financeiro']").val((totalbruto).toFixed(2));
	}
}
$("input[name='in-desconto']").change(function(){
	calcTotalLiquido();
	var desconto = $(this).val();
	$("input[name='desconto']").val(desconto);
});
function calcSubTotal(div){
	var esforco = div.find("input[name='esforco[]']").val();
	var valor = div.find("input[name='valor[]']").val();
	if(valor==0){
		valor='0.00';
	}
	div.find("input[name='valor[]']").val(valor);
	var total = (esforco*valor).toFixed(2);
	div.find("input[name='subtotal']").val(total);
	calcTotalLiquido();	
}
$(".linhas").on("change", "input[name='esforco[]'],input[name='valor[]']", function(){
	var input = $(this);
	var div = input.closest('.item-servico');
	calcSubTotal(div);
});
$("button[name='nova-linha']").click(function(){
	var linhas = $(".linhas");
	var servico = linhas.children().first().clone();
	servico.find('input,select').val('');
	servico.find("input[name='subtotal']").val('0.00');
	linhas.append(servico);
});
$(".linhas").on("click", ".remover-linha", function(){
	var div = $(this).closest('.item-servico');
	if($('.item-servico').length > 1){
		div.remove();
	}else{
		div.find('input,select').val('');
		div.find("input[name='subtotal']").val('0.00');
	}
	calcTotalLiquido();
});
$(".linhas").on("change", "input[name='servico[]']", function(){
	var input = $(this);
	var datalist = $("#servico");
	datalist.children().each(function(key, option){
		if(input.val() === option.value){
			input.data('valor',option.dataset.valor);
			input.data('equipe',option.dataset.equipe);
			input.data('selected',option.innerHTML);
			return false;
		}
	});
	var div = input.closest('.item-servico');
	div.find("input[name='esforco[]']").val('1');
	var valor = input.data('valor');
	var equipe = input.data('equipe');

	if(valor > 0){
		div.find("input[name='valor[]']").val(valor);
		div.find("select[name='equipe[]']").val(equipe).change();
		calcSubTotal(div);
	}else{
		div.find("select[name='equipe[]']").val(equipe);
		div.find("input[name='valor[]']").val('0.00');
		calcSubTotal(div);
	}
});
/*fim orçamento view*/
		/* BUSCA DE ENDEREÇO COM O CEP */
		$("input[name='cep']").change(function(){
            $.getJSON("//viacep.com.br/ws/"+this.value+"/json/?callback=?", function(dados) {
                if (!("erro" in dados)) {
                    $("input[name='endereco']").val(dados.logradouro);
                    $("input[name='bairro']").val(dados.bairro);
                    $("select[name='estado']").val(dados.uf).change();
                    $("select[name='cidade']").val($('option:contains("'+dados.localidade+'")').val()).change();
                }else{
                    alert("CEP não encontrado.");
                }
            });
		});
		/* BUSCA DE ENDEREÇO COM O CEP */
		function cpf_validate(value){
            value = value.replace(/[^\d]+/g,'');
            if(value === '') return false;
            // Elimina CPFs invalidos conhecidos
            if (value.length != 11 ||
                value == "00000000000" ||
                value == "11111111111" ||
                value == "22222222222" ||
                value == "33333333333" ||
                value == "44444444444" ||
                value == "55555555555" ||
                value == "66666666666" ||
                value == "77777777777" ||
                value == "88888888888" ||
                value == "99999999999")
                return false;
            // Valida 1o digito
            var add = 0, i;
            for (i=0; i < 9; i ++)
                add += parseInt(value.charAt(i)) * (10 - i);
            rev = 11 - (add % 11);
            if (rev == 10 || rev == 11)
                rev = 0;
            if (rev != parseInt(value.charAt(9)))
                return false;
            // Valida 2o digito
            add = 0;
            for (i = 0; i < 10; i ++)
                add += parseInt(value.charAt(i)) * (11 - i);
            var rev = 11 - (add % 11);
            if (rev == 10 || rev == 11)
                rev = 0;
            if (rev != parseInt(value.charAt(10)))
                return false;

            return true;
        }
        function cnpj_validate(cnpj) {

		    cnpj = cnpj.replace(/[^\d]+/g,'');
		 
		    if(cnpj == '') return false;
		     
		    if (cnpj.length != 14)
		        return false;
		 
		    // Elimina CNPJs invalidos conhecidos
		    if (cnpj == "00000000000000" || 
		        cnpj == "11111111111111" || 
		        cnpj == "22222222222222" || 
		        cnpj == "33333333333333" || 
		        cnpj == "44444444444444" || 
		        cnpj == "55555555555555" || 
		        cnpj == "66666666666666" || 
		        cnpj == "77777777777777" || 
		        cnpj == "88888888888888" || 
		        cnpj == "99999999999999")
		        return false;
		         
		    // Valida DVs
		    tamanho = cnpj.length - 2
		    numeros = cnpj.substring(0,tamanho);
		    digitos = cnpj.substring(tamanho);
		    soma = 0;
		    pos = tamanho - 7;
		    for (i = tamanho; i >= 1; i--) {
		      soma += numeros.charAt(tamanho - i) * pos--;
		      if (pos < 2)
		            pos = 9;
		    }
		    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		    if (resultado != digitos.charAt(0))
		        return false;
		         
		    tamanho = tamanho + 1;
		    numeros = cnpj.substring(0,tamanho);
		    soma = 0;
		    pos = tamanho - 7;
		    for (i = tamanho; i >= 1; i--) {
		      soma += numeros.charAt(tamanho - i) * pos--;
		      if (pos < 2)
		            pos = 9;
		    }
		    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		    if (resultado != digitos.charAt(1))
		          return false;
		           
		    return true;
		}
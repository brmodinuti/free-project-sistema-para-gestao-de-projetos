/*
BANCO DE DADOS DO SISTEMA FREE PROJECT 1.0
AUTOR: BRUNO MODINUTI
DT CRIAÇÃO: 04/10/2017
DT FINALIZAÇÃO: 04/11/2017
E-MAIL: BRMODINUTI@GMAIL.COM
WWW.FREEPROJECT.COM.BR
*/

drop database if exists freeproject;
create database freeproject;
use freeproject;

create table usuario (
	id_usuario int not null auto_increment, 
	tp_pessoa varchar(2),
    nome varchar(60),
    nome_fantasia varchar(60),
    razao_social varchar(60),
    cpf varchar(20),
    cnpj varchar(20),
    rg varchar(20),
    dt_nascimento varchar(10),
    ie varchar(20),
    tel_residencial varchar(20),
    tel_celular varchar(20),
    tel_comercial varchar(20),
    tel_comercial_ramal varchar(6),
    tel_recado varchar(20),
    cep varchar(20),
    endereco varchar(60),
    numero varchar(20),
    bairro varchar(20),
    complemento varchar(20),
    estado varchar(2),
    cidade varchar(20),
    tp_usuario varchar(15), --CL, PR, AD
    email varchar(60),
    senha varchar(60),
    primary key (id_usuario)
);

create table administrador(
	id_administrador int not null auto_increment,
    fk_usuario int not null,
    primary key (id_administrador),
    foreign key (fk_usuario) references usuario (id_usuario)
);

create table cliente(
	id_cliente int not null auto_increment,
    fk_usuario int not null,
    primary key (id_cliente),
    foreign key (fk_usuario) references usuario (id_usuario)
);

create table orcamento (
    id_orcamento int not null auto_increment,
    fk_cliente int not null,
    titulo varchar(45) not null,
    dt_criacao datetime not null default current_timestamp,
    primary key (id_orcamento),
    foreign key (fk_cliente) references cliente (id_cliente)
);

create table servico (
    id_servico int not null auto_increment,
    nome varchar(45) not null,
    preco decimal(10,2) not null,
    primary key (id_servico)
);

create table orcamento_servico (
    id_orcamento_servico int not null auto_increment,
    fk_orcamento int not null,
    fk_servico int not null,
    quantidade decimal(10,2) not null default 1,
    valor decimal(10,2) not null,
    primary key (id_orcamento_servico),
    foreign key (fk_orcamento) references orcamento (id_orcamento),
    foreign key (fk_servico) references servico (id_servico)
);

create table equipe (
	id_equipe int not null auto_increment,
    nome varchar(60) not null,
    primary key (id_equipe)
);

create table profissional(
	id_profissional int not null auto_increment,
    fk_usuario int not null,
    fk_equipe int not null,
    primary key (id_profissional),
    foreign key (fk_usuario) references usuario (id_usuario),
    foreign key (fk_equipe) references equipe (id_equipe)
);

create table solicitacao (
	id_solicitacao int not null auto_increment,
    fk_cliente int not null,
    titulo varchar(60) not null,
    descricao text not null,
    dt_criacao datetime not null default current_timestamp,
    primary key (id_solicitacao),
    foreign key (fk_cliente) references cliente (id_cliente)
);

create table solicitacao_resposta (
	id_solicitacao_resposta int not null auto_increment,
	fk_solicitacao int not null,
	fk_profissional int not null,
	descricao text not null,
	dt_criacao datetime not null default current_timestamp,
	primary key (id_solicitacao_resposta),
	foreign key (fk_solicitacao) references solicitacao (id_solicitacao),
	foreign key (fk_profissional) references profissional (id_profissional)
);

create table projeto(
	id_projeto int not null auto_increment,
    fk_cliente int not null,
    fk_orcamento int not null,
    titulo varchar(60) not null,
    descricao text not null,
    dt_criacao datetime not null default current_timestamp,
    dt_previsao date,
    dt_conclusao date,
    primary key (id_projeto),
    foreign key (fk_cliente) references cliente (id_cliente),
    foreign key (fk_orcamento) references orcamento (id_orcamento)
);

create table situacao (
	id_situacao int not null auto_increment,
    nome varchar(20) not null,
    primary key (id_situacao)
);

create table tarefa (
	id_tarefa int not null auto_increment,
    fk_projeto int not null,
    titulo varchar(60) not null,
    fk_profissional int not null,
    fk_situacao int not null,
    dt_criacao datetime not null default current_timestamp,
    dt_previsao date,
    dt_conclusao date,
    primary key (id_tarefa),
    foreign key (fk_projeto) references projeto (id_projeto),
    foreign key (fk_profissional) references profissional (id_profissional),
    foreign key (fk_situacao) references situacao (id_situacao)
);

create table andamento (
	id_andamento int not null auto_increment,
    fk_tarefa int not null,
    fk_profissional int not null,
    descricao text not null,
    dt_criacao datetime not null default current_timestamp,
    primary key (id_andamento),
    foreign key (fk_tarefa) references tarefa (id_tarefa),
    foreign key (fk_profissional) references profissional (id_profissional)
);

create table conta (
    id_conta int not null auto_increment,
    nome varchar(45) not null,
    primary key (id_conta)
);

create table categoria (
    id_categoria int not null auto_increment,
    nome varchar(45) not null,
    primary key (id_categoria)
);

create table movimento_financeiro (
    id_movimento_financeiro int not null auto_increment,
    fk_conta int not null,
    fk_categoria int not null,
    fk_projeto int not null,
    titulo varchar(45) not null,
    descricao text,
    valor_pagar decimal(10,2) not null,
    valor_pagamento decimal(10,2),
    data_processamento date,
    data_pagar date not null,
    data_pagamento date,
    primary key (id_movimento_financeiro),
    foreign key (fk_conta) references conta (id_conta),
    foreign key (fk_categoria) references categoria (id_categoria),
    foreign key (fk_projeto) references projeto (id_projeto)
);
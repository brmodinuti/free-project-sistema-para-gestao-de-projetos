<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Clientes_model','clientes');
		$this->load->model('Cidades_model','cidades');
		$this->load->model('Servicos_model','servicos');
		$this->load->model('Orcamentos_model','orcamentos');
		$this->load->model('Itens_orcamentos_model','itens_orcamentos');
		$this->load->model('Projetos_model','projetos');
		$this->load->model('Equipes_model','equipes');
		$this->load->model('Tarefas_model','tarefas');
		$this->load->model('Financeiro_model','financeiro');
	}

	public function index(){
		$dados['titulo'] = 'Início';
		$this->load->view('dashboard', $dados);
	}

	public function inicio(){
		$dados['titulo'] = 'Início';
		$this->load->view('dashboard', $dados);
	}
/* CLIENTES INICIO */
	/* View para mostrar todos os clientes cadastrados */
	public function cliente_list(){
		//Executa função da model
		$dados['clientes'] = $this->clientes->select();
		//Define o título da página
		$dados['titulo'] = 'Clientes';
		//Carrega a view
		$this->load->view('clientes-list', $dados);
	}
	/* View para mostrar informações do cliente selecionado */
	public function cliente_selected($id){
		//Recupera dados do clientes
		$dados['cliente'] = $this->clientes->select($id);
		//Recupera dados de cidades
		$dados['cidades'] = $this->cidades->select();
		//Define o título da página
		$dados['titulo'] = 'Cliente '.$id;
		//Carrega a view
		$this->load->view('clientes-view',$dados);
	}
	/* View para novo cliente */
	public function cliente_new(){
		//Recupera dados de cidades
		$dados['cidades'] = $this->cidades->select();
		//Define o título da página
		$dados['titulo'] = 'Novo Cliente';
		//Carrega a view
		$this->load->view('clientes-view', $dados);
	}
	/* Inserir novo cliente */
	public function cliente_insert(){
		//Pega os dados do formulário
		$dados_form['nome'] 				= $this->input->post('nome');
		$dados_form['tp_pessoa'] 			= $this->input->post('tipopessoa');
		$dados_form['nome_fantasia'] 		= $this->input->post('nome_fantasia');
		$dados_form['razao_social'] 		= $this->input->post('razaosocial');
		$dados_form['cpf'] 					= $this->input->post('cpf');
		$dados_form['cnpj'] 				= $this->input->post('cnpj');
		$dados_form['rg'] 					= $this->input->post('rg');
		$dados_form['dt_nascimento'] 		= $this->input->post('dt_nascimento');
		$dados_form['ie'] 					= $this->input->post('ie');
		$dados_form['tel_residencial'] 		= $this->input->post('tel_residencial');
		$dados_form['tel_celular'] 			= $this->input->post('tel_celular');
		$dados_form['tel_comercial'] 		= $this->input->post('tel_comercial');
		$dados_form['tel_comercial_ramal'] 	= $this->input->post('ramal');
		$dados_form['tel_recado'] 			= $this->input->post('tel_recado');
		$dados_form['cep'] 					= $this->input->post('cep');
		$dados_form['endereco'] 			= $this->input->post('endereco');
		$dados_form['numero'] 				= $this->input->post('numero');
		$dados_form['bairro'] 				= $this->input->post('bairro');
		$dados_form['complemento'] 			= $this->input->post('complemento');
		$dados_form['fk_cidade'] 			= $this->input->post('cidade');
		$dados_form['email'] 				= $this->input->post('email');
		//Envia os dados recuperados para a model
		$cliente = $this->clientes->insert($dados_form);
		
		//Retorna resultado da execução
		if($cliente === true)
			echo json_encode(array('status'=>true));
		else
			echo json_encode(array('status'=>false));
	}
	/* Atualizar dados do cliente selecionado */
	public function cliente_update(){
		//Pega os dados do formulário
		$dados_form['id_cliente'] 			= $this->input->post('id');
		$dados_form['nome'] 				= $this->input->post('nome');
		$dados_form['tp_pessoa'] 			= $this->input->post('tipopessoa');
		$dados_form['nome_fantasia'] 		= $this->input->post('nome_fantasia');
		$dados_form['razao_social'] 		= $this->input->post('razaosocial');
		$dados_form['cpf'] 					= $this->input->post('cpf');
		$dados_form['cnpj'] 				= $this->input->post('cnpj');
		$dados_form['rg'] 					= $this->input->post('rg');
		$dados_form['dt_nascimento'] 		= $this->input->post('dt_nascimento');
		$dados_form['ie'] 					= $this->input->post('ie');
		$dados_form['tel_residencial'] 		= $this->input->post('tel_residencial');
		$dados_form['tel_celular'] 			= $this->input->post('tel_celular');
		$dados_form['tel_comercial'] 		= $this->input->post('tel_comercial');
		$dados_form['tel_comercial_ramal'] 	= $this->input->post('ramal');
		$dados_form['tel_recado'] 			= $this->input->post('tel_recado');
		$dados_form['cep'] 					= $this->input->post('cep');
		$dados_form['endereco'] 			= $this->input->post('endereco');
		$dados_form['numero'] 				= $this->input->post('numero');
		$dados_form['bairro'] 				= $this->input->post('bairro');
		$dados_form['complemento'] 			= $this->input->post('complemento');
		$dados_form['fk_cidade'] 			= $this->input->post('cidade');
		$dados_form['email'] 				= $this->input->post('email');
		//Envia os dados recuperados para a model
		$cliente = $this->clientes->update($dados_form['id_cliente'],$dados_form);
		//Retorna resultado da execução
		if($cliente === true)
			echo json_encode(array('status'=>true));
		else
			echo json_encode(array('status'=>false));
	}
	/* Remove o cliente do banco de dados */
	public function cliente_delete(){
		$id = $this->input->post('id');
		$orcamentos = $this->orcamentos->select_where('fk_cliente',$id);
		foreach ($orcamentos as $i) {
			$this->itens_orcamentos->delete_where('fk_orcamento', $i->id_orcamento);
			$this->orcamentos->delete_where('id_orcamento', $i->id_orcamento);
		}
		$return = $this->clientes->delete($id);
		if($return === true)
			echo json_encode(array('status'=>true));
		else
			echo json_encode(array('status'=>false));
	}
/* CLIENTES FIM */
/* ORCAMENTOS INICIO */
	public function orcamento_list(){
		$dados['orcamentos'] = $this->orcamentos->select();
		$dados['titulo'] = 'Orçamentos';
		$this->load->view('orcamentos-list', $dados);
	}
	public function orcamento_selected($id){
		$dados['orcamento'] = $this->orcamentos->select($id);
		$dados['clientes'] = $this->clientes->select();
		$dados['servicos'] = $this->servicos->select();
		$dados['equipes'] = $this->equipes->select();
		// print_r($dados['orcamento']);
		$dados['itens'] = $this->itens_orcamentos->select($id);
		// echo '<pre>';
		// var_dump($dados['itens']);
		// die;
		$dados['titulo'] = 'Orçamento '.$id;
		//Define a operação do formulário
		//Carrega a view
		$this->load->view('orcamentos-view',$dados);
	}
	public function orcamento_new(){
		//Recupera todos os clientes
		$dados['clientes'] = $this->clientes->select();
		$dados['servicos'] = $this->servicos->select();
		$dados['equipes'] = $this->equipes->select();
		$dados['itens'] = [null];
		// print_r($dados['equipes']);
		//Recupera todos os serviços
		$dados['titulo'] = 'Novo Orçamento';
		$this->load->view('orcamentos-view', $dados);
	}
	public function orcamento_insert(){
		//Dados do orçamento
		$orcamento = array(
			'fk_cliente' => $this->input->post('cliente'),
			'dt_criacao' => $this->input->post('dt_criacao'),
			'dt_validade' => $this->input->post('dt_validade'),
			'dt_entrega' => $this->input->post('dt_entrega'),
			'situacao' => $this->input->post('situacao'),
			'titulo' => $this->input->post('titulo'),
			'descricao' => $this->input->post('descricao'),
			'desconto'=> $this->input->post('desconto'),
			);
		$id_orcamento = $this->orcamentos->insert($orcamento);
		//Dados de todos os itens incluidos no orçamento
		$qt_itens = count($this->input->post('servico[]'));
		for($i=0; $i<$qt_itens; $i++){
			$itens = array(
				'fk_orcamento' => $id_orcamento,
				'fk_equipe' => $this->input->post('equipe['.$i.']'),
				'nome' => $this->input->post('servico['.$i.']'),
				'esforco' => $this->input->post('esforco['.$i.']'),
				'preco' => $this->input->post('valor['.$i.']'),
				);
			$this->itens_orcamentos->insert($itens);
		}
		echo json_encode($id_orcamento);
	}
	public function orcamento_update(){
		$id_orcamento = $this->input->post('cod');
		$orcamento = array(
			'fk_cliente' 	=> $this->input->post('cliente'),
			'dt_criacao' 	=> $this->input->post('dt_criacao'),
			'dt_validade' 	=> $this->input->post('dt_validade'),
			'dt_entrega' 	=> $this->input->post('dt_entrega'),
			'situacao' 		=> $this->input->post('situacao'),
			'titulo' 		=> $this->input->post('titulo'),
			'descricao' 	=> $this->input->post('descricao'),
			'desconto'		=> $this->input->post('desconto'),
			);
		//Atualiza os dados do orçamento no banco de dados
		$this->orcamentos->update($id_orcamento,$orcamento);
		//Array para separar itens que precisam ser atualizados ou removidos
		$itens_pagina 	= array();
		$itens_banco 	= array();
		$resultQuery 	= $this->itens_orcamentos->select($id_orcamento);
		$qtItensPagina 	= count($this->input->post('servico[]'));
		//Guarda os itens da página em um array
		for($i=0; $i<$qtItensPagina; $i++){
			//Guarda itens existentes na página em array para comparações
			array_push($itens_pagina, $this->input->post('id_item['.$i.']'));
			//Inclui novo item
			if($this->input->post('id_item['.$i.']') == '' && $this->input->post('servico['.$i.']') != ''){
				$item = array(
					'fk_orcamento' 	=> $id_orcamento,
					'fk_equipe' 	=> $this->input->post('equipe['.$i.']'),
					'nome' 			=> $this->input->post('servico['.$i.']'),
					'esforco' 		=> $this->input->post('esforco['.$i.']'),
					'preco' 		=> $this->input->post('valor['.$i.']'),
					);
				$this->itens_orcamentos->insert($item);
			}
		}
		//Guarda os itens salvos no banco de dados
		foreach ($resultQuery as $i){
			array_push($itens_banco, $i->id_item);
		}
		//Guarda itens que constam na página e no banco de dados
		$updateItens = array_intersect($itens_banco,$itens_pagina);
		//Guarda itens que não constam na página e consta no banco de dados
		$deleteItens = array_diff($itens_banco,$itens_pagina);
		// //Atualiza itens
		foreach ($updateItens as $i){
			for($x=0; $x<count($updateItens);$x++){
				$item = array(
					'id_item' 	=> $this->input->post('id_item['.$x.']'),
					'fk_equipe' => $this->input->post('equipe['.$x.']'),
					'nome' 		=> $this->input->post('servico['.$x.']'),
					'esforco' 	=> $this->input->post('esforco['.$x.']'),
					'preco' 	=> $this->input->post('valor['.$x.']'),
					);
				$this->itens_orcamentos->update($item['id_item'],$item);
			}
		}
		//Remove itens
		foreach ($deleteItens as $i){
			$this->itens_orcamentos->delete($i);
		}
		echo json_encode($id_orcamento);
	}
	public function orcamento_delete(){
		$id = $this->input->post('cod');
		$this->itens_orcamentos->delete_where('fk_orcamento', $id);
		$return = $this->orcamentos->delete($id);
		if($return === true)
			echo json_encode(array('status'=>true));
		else
			echo json_encode(array('status'=>false));
	}
/* ORCAMENTOS FIM */
/* PROJETOS INICIO */
	public function orcamento_gera_projeto(){
		//Informações do projeto
		$projeto = array(
			'fk_orcamento' 	=> $this->input->post('cod'),
			'fk_cliente' 	=> $this->input->post('cliente'),
			'titulo' 		=> $this->input->post('titulo'),
			'descricao' 	=> $this->input->post('descricao'),
			'dt_prevista' 	=> $this->input->post('dt_entrega')
		);
		//Insere projeto
		$id = $this->projetos->insert($projeto);
		//Informa ao orçamento que foi gerado um projeto
		$this->orcamentos->update_create_orcamento($projeto['fk_orcamento']);
		// //Retorna para a view o id do projeto criado
		$qt_tarefas = count($this->input->post('servico[]'));
		for($i=0; $i<$qt_tarefas; $i++){
			$tarefas = array(
				'fk_projeto' 	=> $id,
				'fk_equipe' 	=> $this->input->post('equipe['.$i.']'),
				'titulo' 		=> $this->input->post('servico['.$i.']')
				);
			$this->tarefas->insert($tarefas);
		}

		$financeiro = array(
			'descricao' => 'Projeto '.$id,
			'valor' => $this->input->post('financeiro'),
			'dt_vencimento' => $this->input->post('dt_entrega') 
		);

		$this->financeiro->insert($financeiro);

		echo json_encode($id);
	}

	public function projeto_list(){
		$dados['projetos'] = $this->projetos->select();
		$dados['titulo'] = 'Projetos';
		$this->load->view('projetos-list', $dados);
	}

	public function projeto_selected($id){
		$dados['projeto'] = $this->projetos->select($id);
		$dados['tarefas'] = $this->tarefas->select_where('fk_projeto',$id);
		$dados['titulo'] = 'Projeto '.$id;
		$qt_itens = count($this->input->post('servico[]'));
		$this->load->view('projetos-view',$dados);
	}
	
/* PROJETOS FIM*/
	public function financeiro(){
		$dados['movimentacoes'] = $this->financeiro->select();
		$dados['titulo'] = 'Financeiro';
		$this->load->view('financeiro-list', $dados);
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tarefas_model extends CI_Model {

	public function select_where($coluna,$id){
		$this->db->select('tarefas.*,equipes.nome');
		$this->db->where($coluna, $id);
		$this->db->join('equipes', 'tarefas.fk_equipe = equipes.id_equipe');
		$query = $this->db->get('tarefas');
		return $query->result();
	}

	public function insert($data){
		$this->db->insert('tarefas', $data);
		return true;
	}

}
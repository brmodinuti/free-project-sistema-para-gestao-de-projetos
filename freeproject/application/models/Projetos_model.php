<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projetos_model extends CI_Model {

	public function select($id=''){
		if($id === ''){
			$this->db->order_by('id_projeto', 'DESC');
			$query = $this->db->get('projetos');
			return $query->result();
		}else{
			$this->db->select('projetos.*,DATE_FORMAT(dt_prevista, "%d/%m/%Y") as dt_prevista,clientes.nome');
			// $this->db->select('projetos.*,clientes.nome');
			$this->db->from('projetos');
			$this->db->where('id_projeto', $id);
			$this->db->join('clientes', 'projetos.fk_cliente = clientes.id_cliente');
			$query = $this->db->get();
			return $query->row();
		}
		
	}

	public function insert($data){
		/* Conversão de datas */
		$dt_prevista = $data['dt_prevista'];
		$temp = explode("/",$dt_prevista);
		$data['dt_prevista'] = $temp[2].'-'.$temp[1].'-'.$temp[0];

		$this->db->insert('projetos', $data);
		$last_id = $this->db->insert_id();
    	return $last_id;
	}
}
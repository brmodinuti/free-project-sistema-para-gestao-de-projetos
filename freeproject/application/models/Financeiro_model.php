<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Financeiro_model extends CI_Model {

	public function select(){
		$this->db->order_by('id_movimentacao', 'DESC');
		$this->db->select('movimentacoes_financeiras.*,DATE_FORMAT(dt_vencimento, "%d/%m/%Y") as dt_vencimento');
		$query = $this->db->get('movimentacoes_financeiras');
		return $query->result();
	}

	public function insert($data){
		/* Conversão de datas */
		$dt_vencimento = $data['dt_vencimento'];
		$temp = explode("/",$dt_vencimento);
		$data['dt_vencimento'] = $temp[2].'-'.$temp[1].'-'.$temp[0];

		$this->db->insert('movimentacoes_financeiras',$data);
		return true;
	}

}
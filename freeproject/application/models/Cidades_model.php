<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cidades_model extends CI_Model {

	public function select(){
		$this->db->order_by('nome', 'ASC');
			$query = $this->db->get('cidades');
			return $query->result();
	}

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes_model extends CI_Model {

	public function select($id=''){
		if($id === ''){
			$this->db->order_by('nome', 'ASC');
			$query = $this->db->get('clientes');
			return $query->result();
		}else{
			$this->db->select('clientes.*,cidades.uf');    
			$this->db->from('clientes');
			$this->db->where('id_cliente', $id);
			$this->db->join('cidades', 'clientes.fk_cidade = cidades.id_cidade');
			$query = $this->db->get();
			return $query->row();
			// $query = $this->db->query("SELECT clientes.*,cidades.uf FROM clientes INNER JOIN cidades ON (clientes.fk_cidade = cidades.id_cidade) WHERE id_cliente=".$id); 
			// return $row = $query->row();
		}
	}
	
	public function insert($data){
		$this->db->insert('clientes', $data);
		return true;
	}

	public function update($id,$data){
		$this->db->where('id_cliente', $id);
		$this->db->update('clientes', $data);
		return true;
	}

	public function delete($id){
		$this->db->where('id_cliente', $id);
		$this->db->delete('clientes');
		return true;
	}

}
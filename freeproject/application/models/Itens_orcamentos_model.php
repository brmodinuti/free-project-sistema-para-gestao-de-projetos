<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Itens_orcamentos_model extends CI_Model {

	public function select($id){
			$this->db->where('fk_orcamento', $id);
			$query = $this->db->get('itens_orcamentos');
			return $query->result();
	}

	public function select_where($coluna,$id){
		$this->db->where($coluna, $id);
		$query = $this->db->get('itens_orcamentos');
		return $query->result();
	}

	public function insert($data){
		$this->db->insert('itens_orcamentos',$data);
		return true;
	}

	public function update($id, $data){
		$this->db->where('id_item', $id);
		$this->db->update('itens_orcamentos', $data);
	}

	public function delete($id){
		$this->db->where('id_item', $id);
		$this->db->delete('itens_orcamentos');
		return true;
	}

	public function delete_where($coluna, $id){
		$this->db->where($coluna, $id);
		$this->db->delete('itens_orcamentos');
		return true;
	}


}
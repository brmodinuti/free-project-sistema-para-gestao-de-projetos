<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Equipes_model extends CI_Model {

	public function select(){
		$this->db->order_by('nome', 'ASC');
			$query = $this->db->get('equipes');
			return $query->result();
	}

}
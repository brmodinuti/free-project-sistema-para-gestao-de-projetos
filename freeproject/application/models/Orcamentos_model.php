<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orcamentos_model extends CI_Model {

	public function select($id=''){
		if($id === ''){
			$this->db->order_by('id_orcamento', 'DESC');
			$query = $this->db->get('orcamentos');
			return $query->result();
		}else{
			$this->db->from('orcamentos');
			$this->db->where('id_orcamento', $id);
			$this->db->select('*,DATE_FORMAT(dt_criacao, "%d/%m/%Y") as dt_criacao,DATE_FORMAT(dt_validade, "%d/%m/%Y") as dt_validade,DATE_FORMAT(dt_entrega, "%d/%m/%Y") as dt_entrega', FALSE);
			$query = $this->db->get();
			return $query->row();
		}
	}

	public function select_where($coluna,$id){
		$this->db->where($coluna, $id);
		$query = $this->db->get('orcamentos');
		return $query->result();
	}

	public function insert($data){
		/* Conversão de datas */
		$dt_criacao = $data['dt_criacao'];
		$temp = explode("/",$dt_criacao);
		$data['dt_criacao'] = $temp[2].'-'.$temp[1].'-'.$temp[0];

		$dt_validade = $data['dt_validade'];
		$temp = explode("/",$dt_validade);
		$data['dt_validade'] = $temp[2].'-'.$temp[1].'-'.$temp[0];

		$dt_entrega = $data['dt_entrega'];
		$temp = explode("/",$dt_entrega);
		$data['dt_entrega'] = $temp[2].'-'.$temp[1].'-'.$temp[0];
		/* Conversão de datas */

		$this->db->insert('orcamentos', $data);
		//retorna o id
		$last_id = $this->db->insert_id();
    	return $last_id;
	}

	public function update($id,$data){
		/* Conversão de datas */
		$dt_criacao = $data['dt_criacao'];
		$temp = explode("/",$dt_criacao);
		$data['dt_criacao'] = $temp[2].'-'.$temp[1].'-'.$temp[0];

		$dt_validade = $data['dt_validade'];
		$temp = explode("/",$dt_validade);
		$data['dt_validade'] = $temp[2].'-'.$temp[1].'-'.$temp[0];

		$dt_entrega = $data['dt_entrega'];
		$temp = explode("/",$dt_entrega);
		$data['dt_entrega'] = $temp[2].'-'.$temp[1].'-'.$temp[0];
		/* Conversão de datas */

		$this->db->where('id_orcamento', $id);
		$this->db->update('orcamentos', $data);
		return true;
	}

	public function update_create_orcamento($id){

		$this->db->where('id_orcamento', $id);
		$this->db->set(array('orcamento' => 1));
		$this->db->update('orcamentos');
		return true;
	}

	public function delete($id){
		//Remove o orçamento
		$this->db->where('id_orcamento', $id);
		$this->db->delete('orcamentos');
		//Retorna TRUE
		return true;
	}

	public function delete_where($coluna, $id){
		//Remove o orçamento
		$this->db->where($coluna, $id);
		$this->db->delete('orcamentos');
		//Retorna TRUE
		return true;
	}

}
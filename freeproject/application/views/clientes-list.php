﻿<?php $this->load->view('header') ?>

		<main class="animated fadeIn">
			<div class="container bc">
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url('inicio')?>">Início</a></li>
					<li class="active">Clientes</li>
				</ol>
			</div>
			<div class="container">
				<div class="row">
					<div class="col col-sm-1 col-xs-2">
						<button name="cliente-novo" class="btn btn-info btn-left">Adicionar</button>
					</div>
					<div class="col col-sm-4 col-sm-offset-7 col-xs-offset-2 col-xs-8">
						<div class="input-group">
							<input name="pesquisa" type="text" class="form-control" placeholder="Nome, CPF, E-mail...">
							<span class="input-group-btn">
								<button name="pesquisar" class="btn btn-default" type="button"><b class="glyphicon glyphicon-search"></b></button>
							</span>
						</div>
					</div>
				</div>
			</div>
			<!-- lista -->
			<div class="container conteudo">
				<div class="row">
					<div class="col col-sm-6 col-xs-12 col-titulo">Cód / Nome</div>
					<div class="col col-sm-3 hidden-xs col-titulo">Telefone</div>
					<div class="col col-sm-3 hidden-xs col-titulo">E-mail</div>
				</div>
				<?php
					$contador=0;
					foreach ($clientes as $i) {
						echo '
						<a href="clientes/'.$i->id_cliente.'" class="link-list">
							<div class="row">
								<div class="col col-sm-6 col-xs-12">'.$i->id_cliente.'. '.$i->nome.'</div>
								<div class="col col-sm-3 hidden-xs">'.$i->tel_celular.'</div>
								<div class="col col-sm-3 hidden-xs">'.$i->email.'</div>
							</div>
						</a>';
						$contador++;
					}
					if($contador == 0){echo '<h2>Nenhum cliente encontrado<h2>';}
				?>
			</div><!-- fim .container .conteudo -->
			<!-- fim lista -->
			<div class="container">
				<nav aria-label="Page navigation">
					<ul class="pagination">
						<li>
							<a href="#" aria-label="Previous">
								<span aria-hidden="true">&laquo;</span>
							</a>
						</li>
						<li class="active"><a href="#">1</a></li>
						<li>
							<a href="#" aria-label="Next">
								<span aria-hidden="true">&raquo;</span>
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</main>

<?php $this->load->view('footer') ?>

</html>
﻿<?php $this->load->view('header') ?>

		<main class="animated fadeIn">
			<div class="container bc">
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url('inicio')?>">Início</a></li>
					<li class="active">Financeiro</li>
				</ol>
			</div>
			
			<div class="container">
				<div class="row">
					<div class="col col-sm-1 col-xs-2">
						<button name="novo" class="btn btn-info btn-left">Adicionar</button>
					</div><!-- .col-sm-2 -->
					<div class="col col-sm-4 col-xs-10">
						<div class="btn-group float-right" role="group" aria-label="...">
							<button type="button" class="btn btn-default"><b class="glyphicon glyphicon-menu-left"></b></button>
							<button type="button" class="btn btn-default">Novembro - 2017</button>
							<button type="button" class="btn btn-default"><b class="glyphicon glyphicon-menu-right"></b></button>
						</div>
					</div><!-- col col-sm-4 -->
					<div class="col col-sm-3 hidden-xs p4l">
						<select class="form-control">
							<option>Todos</option>
							<option>Pendente</option>
							<option>Pago</option>
						</select>
					</div><!-- .col-sm-3-->
					<div class="col col-sm-4 hidden-xs p4l">
						<div class="input-group">
							<input name="pesquisa" type="text" class="form-control" placeholder="Código, Título, Cliente...">
							<span class="input-group-btn">
								<button name="pesquisar" class="btn btn-default" type="button"><b class="glyphicon glyphicon-search"></b></button>
							</span>
						</div><!-- /input-group -->
					</div><!-- .col-sm-4 -->
				</div><!-- .row -->
			</div><!-- .container -->
			<!-- lista -->
			<div class="container conteudo">
				<div class="row">
						<div class="col col-sm-6 col-xs-9 col-titulo">Cód / Título</div>
						<div class="col col-sm-2 hidden-xs col-titulo">Dt. Vencimento</div>
						<div class="col col-sm-2 hidden-xs col-titulo">Valor</div>
						<div class="col col-sm-2 col-xs-3 col-titulo">Situação</div>
				</div>
				<?php
					$contador=0;
					foreach ($movimentacoes as $i) {
						$situacao = '';
						if($i->situacao == 0){
							$situacao = 'Pendente';
						}else{
							$situacao = 'Pago';
						}
						echo '
						<a href="#" class="link-list">
							<div class="row">
								<div class="col col-sm-6 col-xs-9">'.$i->id_movimentacao.'. '.$i->descricao.'</div>
								<div class="col col-sm-2 hidden-xs">'.$i->dt_vencimento.'</div>
								<div class="col col-sm-2 hidden-xs">R$ '.$i->valor.'</div>
								<div class="col col-sm-2 col-xs-3">'.$situacao.'</div>
							</div>
						</a>';
						$contador++;
					}
					if($contador == 0){echo '<h2>Nenhum cliente encontrado<h2>';}
				?>
			</div><!-- fim .container .conteudo -->
			<!-- fim lista -->
			<div class="container">
				
				<nav aria-label="Page navigation">
					<ul class="pagination">
						<li>
							<a href="#" aria-label="Previous">
								<span aria-hidden="true">&laquo;</span>
							</a>
						</li>
						<li class="active"><a href="#">1</a></li>
						<li>
							<a href="#" aria-label="Next">
								<span aria-hidden="true">&raquo;</span>
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</main>

<?php $this->load->view('footer') ?>

</html>
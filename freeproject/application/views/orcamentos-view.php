﻿<?php 
	include_once 'header.php';
	// $data1 = '2017-10-10';
	// $data2 = '2017-10-11';

	// if(strtotime($data1) > strtotime($data2)){ 
	//     echo 'Data 1 maior<br>';
	// }else{
	//     echo 'Data 1 menor<br>';
	// }
	/* Carrega data atual */
	$dt_criacao 	= date('d/m/Y');
	$dt_validade 	= date('d/m/Y', strtotime('+10 days'));
	$dt_entrega 	= date('d/m/Y', strtotime('+40 days'));
?>
		<main class="animated fadeIn">
			<div class="container bc">
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url('inicio')?>">Início</a></li>
					<li><a href="<?php echo base_url('orcamentos')?>">Orçamentos</a></li>
					<li class="active"><?php echo empty($orcamento->id_orcamento)?'Novo':$orcamento->id_orcamento;?></li>
				</ol>
			</div>
			<div class="mensagens container"></div>
			<div class="container conteudo">
				<!-- inicio descrição do orçamento -->
				<form class="form-orcamento">
					<div class="row">
						<div class="col col-sm-2 col-xs-6 col-input">
							<label>Cód. Orçamento</label><br/>
							<input type="text" name="cod" class="form-control" 
							value="<?php echo empty($orcamento->id_orcamento)?'Novo':$orcamento->id_orcamento;?>" disabled>
							<input type="text" name="cod" hidden value="<?php echo empty($orcamento->id_orcamento)?'Novo':$orcamento->id_orcamento;?>">
						</div>
						<div class="col col-sm-2 col-xs-6 col-sm-offset-2 col-input">
							<label>Dt. Criação</label><br/>
							<input type="text" name="dt_criacao" class="form-control data maskDATA req" 
							value="<?php echo empty($orcamento->dt_criacao)? $dt_criacao :$orcamento->dt_criacao;?>">
						</div>
						<div class="col col-sm-2 col-xs-6 col-input">
							<label>Dt. Validade</label><br/>
							<input type="text" name="dt_validade" class="form-control data maskDATA req" 
							value="<?php echo empty($orcamento->dt_validade)? $dt_validade :$orcamento->dt_validade;?>">
						</div>
						<div class="col col-sm-2 col-xs-6 col-input">
							<label>Dt. Entrega</label><br/>
							<input type="text" name="dt_entrega" class="form-control data maskDATA req" 
							value="<?php echo empty($orcamento->dt_entrega)? $dt_entrega :$orcamento->dt_entrega;?>">
						</div>
						<div class="col col-sm-2 col-xs-12 col-input">
							<label>Situação</label><br/>
							<select class="form-control" name="situacao">
								<option value="1" selected>Pendente</option>
								<option value="1" <?= (isset($orcamento->situacao) && $orcamento->situacao == 1 ? 'selected' : '') ?> >Pendente</option>
								<option value="2" <?= (isset($orcamento->situacao) && $orcamento->situacao == 2 ? 'selected' : '') ?> >Aprovado</option>
								<option value="3" <?= (isset($orcamento->situacao) && $orcamento->situacao == 3 ? 'selected' : '') ?> >Recusado</option>
							</select>
						</div>
						<div class="col col-sm-6 col-xs-12 col-input">
							<label>Cliente</label><br/>
							<select class="form-control req placeholder" name='cliente' data-live-search="true" title="Selecione">
								<option value="" selected>Cliente*</option>
								<?php 
									foreach ($clientes as $i) {
										if(isset($orcamento->fk_cliente) && $i->id_cliente==$orcamento->fk_cliente){
											echo '<option value="'.$i->id_cliente.'" selected>'.$i->nome.'</option>';
										}else{
											echo '<option value="'.$i->id_cliente.'">'.$i->nome.'</option>';
										}	
									}
								?>
							</select>
						</div>
						<div class="col col-sm-6 col-xs-12 col-input">
							<label>Título</label><br/>
							<input type="text" name="titulo" class="form-control req" placeholder="Título do orçamento"
							value="<?php echo empty($orcamento->titulo)?'':$orcamento->titulo;?>">
						</div>
						<div class="col col-sm-12 col-xs-12 col-input">
							<label>Descrição</label><br/>
							<textarea name="descricao" rows="5" class="form-control req" placeholder="Descreva o que será feito..."><?php echo empty($orcamento->descricao)?'':$orcamento->descricao;?></textarea>
						</div>
					</div>
					<!-- fim descrição do orçamento -->
					<!-- inicio cabeçalho de serviços -->
					<div class="row cabecalho-servicos">
						<div class="col col-sm-3 hidden-xs col-input">
							<label>Serviço</label>
						</div>
						<div class="col col-sm-3 hidden-xs col-input">
							<label>Equipe</label>
						</div>
						<div class="col col-sm-2 hidden-xs col-input">
							<label>Esforço</label>
						</div>
						<div class="col col-sm-2 hidden-xs col-input">
							<label>Valor</label>
						</div>
						<div class="col col-sm-2 hidden-xs col-input">
							<label>Subtotal</label>
						</div>
					</div>
					<!-- fim cabeçalho de serviços -->
					<!-- inicio lista de serviços -->
					<div class="row linhas" id="itens">
						<?php foreach ($itens as $item): ?>
							<div class="item-servico">
								<div class="col col-sm-12">
									<input type="number" name="id_item[]" hidden value="<?= (isset($item->id_item) ? $item->id_item : '') ?>"/>
								</div>
								<div class="col col-sm-3 col-xs-6 col-input">
									<input type="search" name="servico[]" class="form-control req" list="servico" placeholder="Selecione ou insira um item" value="<?= (isset($item->nome) ? $item->nome : '') ?>">
								</div>
								<div class="col col-sm-3 col-xs-6 col-input">
									<select class="form-control req placeholder" name="equipe[]">
										<option value="" selected>Selecione uma equipe</option>
										<?php foreach ($equipes as $equipe): ?>
											<option value="<?= $equipe->id_equipe ?>" <?= (isset($item->fk_equipe) && $equipe->id_equipe == $item->fk_equipe ? 'selected' : ''); ?> ><?= $equipe->nome ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="col col-sm-2 col-xs-4 col-input">
									<input type="number" name="esforco[]" class="form-control req" value="<?= (isset($item->esforco) ? $item->esforco : '') ?>" placeholder="Esforço">
								</div>
								<div class="col col-sm-2 col-xs-4 col-input">
									<input type="number" name="valor[]" class="form-control req" value="<?= (isset($item->preco) ? $item->preco : '') ?>" placeholder="Valor">
								</div>
								<div class="col col-sm-2 col-xs-4 col-input">
									<input type="number" name="subtotal" value="0.00" class="form-control req" disabled value="">
								</div>
								<small class="remover-linha">x</small>
							</div>
						<?php endforeach; ?>
					</div>
					<datalist id="servico">
						<?php 
							foreach ($servicos as $i) {
								echo '<option data-valor="'.$i->preco.'" data-equipe="'.$i->fk_equipe.'" value="'.$i->nome.'">'.$i->id_servico.'</option>';
							}
						?>
					</datalist>
					<input type="number" name="desconto"
					value="<?php 
						if(isset($orcamento->desconto)){$desconto=number_format($orcamento->desconto, 2, '.', '');}
						echo empty($orcamento->desconto)?'0.00':$desconto; 
						?>"" hidden>
					<input type="number" name="financeiro" value="0.00" hidden>
					</form>
					<!-- fim lista de serviços -->
					<!-- inicio botão adicionar nova linha -->
					<div class="row">	
						<div class="col col-sm-12">
							<button name="nova-linha" class="btn m6b"><b class="glyphicon glyphicon-plus-sign"></b> Adicionar nova linha</button>
						</div>
					</div>
					<!-- fim botão adicionar nova linha -->
					<!-- inicio totais -->
					<div class="row">
						<div class="col col-sm-6 col-sm-offset-6 totais">
							<div class="col col-sm-4 col-xs-4 col-input">
								<label>Valor total</label>
								<input type="number" class="form-control" name="totalbruto" value="0.00" disabled>
							</div>					
							<div class="col col-sm-4 col-xs-4 col-input">
								<label>Desconto</label>
								<input type="number" name="in-desconto" class="form-control" 
								value="<?php 
									if(isset($orcamento->desconto)){$desconto=number_format($orcamento->desconto, 2, '.', '');}
									echo empty($orcamento->desconto)?'0.00':$desconto; 
									?>">
							</div>
							<div class="col col-sm-4 col-xs-4 col-input">
								<label>Total líquido</label>
								<input type="number" name="totalliquido" class="form-control" value="0.00" disabled>
							</div>
						</div>
					</div>
					<!-- fim totais -->
				
				<!-- inicio botoes -->
				<div class="row">
					<div class="col col-sm-12 col-botoes align-right botoes">
						<?php 
							if(isset($orcamento->situacao)){
								if($orcamento->situacao == 2){
									if($orcamento->orcamento == 0){
										echo '<button name="gerarprojeto" class="btn btn-info float-left">Gerar Projeto</button>';
									}else{
										echo '<button class="btn btn-info float-left" disabled>Gerar Projeto</button>';
									}
									echo '<button name="remover" class="btn btn-danger" data-toggle="modal" data-target="#ModalConfirmacao">Remover</button>';
								}else{
									echo '
									<button name="remover" class="btn btn-danger" data-toggle="modal" data-target="#ModalConfirmacao">Remover</button>
									<button name="atualizar" class="btn btn-info">Atualizar</button>';
								}	
							}else{
								echo '
									<button name="salvar" class="btn btn-info">Salvar</button>';
							}
						?>
					</div>
				</div>
				<!-- fim botoes -->
			</div>
		</main>

		<!-- Modal de confirmação -->
		<div class="modal fade" id="ModalConfirmacao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">Confirmação</h4>
		      </div>
		      <div class="modal-body">
		        Realmente deseja remover este orçamento? O projeto gerado por este orçamento será perdido.
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
		        <button type="button" name="confirmar" class="btn btn-danger">Sim</button>
		      </div>
		    </div>
		  </div>
		</div>
		<!-- Fim modal de confirmação -->

<?php include_once 'footer.php';?>

	<script>
		$("button[name='salvar']").click(function(){
			var x = 0;
			$('.req').each(function(i,element){
				element = $(element);
				var expressao = element.is(":visible") && element.val() == "";
				element.toggleClass('required',expressao);
				if(expressao == true){
					x++;
				}
			});
			if(x>0){
				alert_message(0);
			}else{
				$.ajax({
					type: 'POST',
		    		url: '<?php echo base_url('welcome/orcamento_insert')?>',
		    		data: $(".form-orcamento").serialize(),
		    		dataType: 'json',
		    		success: function(id){
		    			$("button[name='salvar']").attr('disabled','disabled');
		    			alert_message(1);
		    			setTimeout(function(){
							window.location.replace('<?php echo base_url('orcamentos')?>');
						},5800);
					},
					error: function(){
						alert_message(0);
					}
		    	});
			}
		});
		$("button[name='atualizar']").click(function(){
			var x = 0;
			$('.req').each(function(i,element){
				element = $(element);
				var expressao = element.is(":visible") && element.val() == "";
				element.toggleClass('required',expressao);
				if(expressao == true){
					x++;
				}
			});
			if(x>0){
				console.log('aqui');
				alert_message(0);
			}else{
				$.ajax({
					type: 'POST',
		    		url: '<?php echo base_url('welcome/orcamento_update')?>',
		    		data: $(".form-orcamento").serialize(),
		    		dataType: 'json',
		    		success: function(id){
		    			// console.log(id);
		    			$("button[name='atualizar'],button[name='remover'],button[name='gerarprojeto']").attr('disabled','disabled');
		    			alert_message(1);
		    			setTimeout(function(){
		    				// location.reload();
							window.location.replace('<?php echo base_url('orcamentos')?>');
						},5800);
					},
					error: function(){
						alert_message(0);
					}
		    	});
			}
		});
		/* Delete */
		$("button[name='confirmar']").click(function(){
			$.ajax({
				method: 'POST',
	    		url: '<?php echo base_url('welcome/orcamento_delete')?>',
	    		data: $('.form-orcamento').serialize(),
	    		dataType: 'json',
	    		success: function(data){
	    			console.log(data);
	    			if(data.status == true){
	    				$("button[name='atualizar'],button[name='remover'],button[name='gerarprojeto']").attr('disabled','disabled');
	    				$('#ModalConfirmacao').modal('toggle');
	    				alert_message(1);
	    				setTimeout(
						function(){
							window.location.replace('<?php echo base_url('orcamentos')?>');
						},5800);
	    			}
	    		},
	    		error: function(){
	    			alert_message(0);
	    		}
	    	});
		});
		/* Gerar Projeto */
		$("button[name='gerarprojeto']").click(function(){
			var situacao = $("select[name='situacao']").val();
			if(situacao == 2){
				$.ajax({
				method: 'POST',
	    		url: '<?php echo base_url('welcome/orcamento_gera_projeto')?>',
	    		data: $('.form-orcamento').serialize(),
	    		dataType: 'json',
	    		success: function(id){
	    			console.log(id);
    				$("button[name='gerarprojeto'],button[name='remover']").attr('disabled','disabled');
    				alert_message(1);
    				setTimeout(
					function(){
						window.location.replace('<?php echo base_url('projetos')?>/'+id);
					},5800);
	    		},
	    		error: function(){
	    			alert_message(0);
	    		}
	    	});
			}
		});
	</script>

</html>
<!DOCTYPE html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="Content-Language" content="pt-br">
        <meta name="viewport" content="width=device-width, user-scalable=yes" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="format-detection" content="telephone=yes" />
        <link rel="icon" href="<?php echo base_url('assets/img/icon.png')?>" type="image/x-icon" />
		<link rel="shortcut icon" href="<?php echo base_url('assets/img/icon.png')?>" type="image/x-icon" />
		<title>Free Project - <?php echo $titulo ?></title>
		<link rel="stylesheet" href="<?php echo base_url('assets/css/animate.css')?>" />
		<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css')?>" />
		<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-select.min.css')?>" />
		<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datepicker.min.css')?>" />
		<link rel="stylesheet" href="<?php echo base_url('assets/css/free.css')?>" />
	</head>
	<body>
		<!-- MENU -->
		<header>
			<div class="container">
				<div class="row visible-xs">
					<div class="col col-xs-2">
						<button id="super-botao"><b class="glyphicon glyphicon-menu-hamburger"></b></button>
					</div>
					<div class="col col-xs-10">
						<ul>
							<a href="<?php echo base_url('inicio')?>"><li id="logo"></li></a>
							<a href="#" id="sair"><li class="float-right"><b class="glyphicon glyphicon-log-in"></b></li></a>
							<a href="#" id="configuracao"><li class="float-right"><b class="glyphicon glyphicon-cog"></b></li></a>
							<a href="#"><li class="float-right">Usuário</li></a>
						</ul>
					</div>
				</div>     
				<ul class="hidden-xs">
					<a href="<?php echo base_url('inicio')?>"><li id="logo"></li></a>
					<a href="<?php echo base_url('inicio')?>"><li>Início</li></a>
					<a href="<?php echo base_url('clientes')?>"><li>Clientes</li></a>
					<a href="<?php echo base_url('orcamentos')?>"><li>Orçamentos</li></a>
					<a href="<?php echo base_url('projetos')?>"><li>Projetos</li></a>
					<a href="<?php echo base_url('financeiro')?>"><li>Financeiro</li></a>
					<a href="#"><li>Relatórios</li></a>
					<a href="<?php echo base_url('sair')?>"><li class="float-right"><b class="glyphicon glyphicon-log-in"></b></li></a>
					<a href="<?php echo base_url('configuracoes')?>"><li class="float-right"><b class="glyphicon glyphicon-cog"></b></li></a>
					<a href="#"><li class="float-right">Usuário</li></a>
				</ul>
			</div>
		</header>
		<div class="super-menu hidden hidden-sm hidden-md hidden-lg">
			<ul class="fadeIn">
				<a href="<?php echo base_url('inicio')?>"><li>Início</li></a>
				<a href="<?php echo base_url('clientes')?>"><li>Clientes</li></a>
				<a href="<?php echo base_url('orcamentos')?>"><li>Orçamentos</li></a>
				<a href="<?php echo base_url('projetos')?>"><li>Projetos</li></a>
				<a href="<?php echo base_url('financeiro')?>"><li>Financeiro</li></a>
				<a href="#"><li>Relatórios</li></a>
			</ul>
		</div>
		<!-- FIM MENU -->
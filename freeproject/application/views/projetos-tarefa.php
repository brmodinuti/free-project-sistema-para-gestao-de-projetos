﻿<?php include_once 'header.php' ?>

		<main class="animated fadeIn">
			<div class="container bc">
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url('inicio')?>">Início</a></li>
					<li><a href="<?php echo base_url('projetos')?>">Projetos</a></li>
					<li><a href="projeto-final.php">1234</a></li>
					<li class="active">1201</li>
				</ol>
			</div>
			<div class="container">
				<div class="row im">
					<div class="col col-sm-2 col-xs-4 im">
						<div class="item-metrica align-center">
							<i>Data Prevista</i><br/>
							<input name="tarefa-previsao" type="text" class="form-control hidden align-center data">
							<b class="tarefa-previsao">10/10/2010</b>
						</div>
					</div>
					<div class="col col-sm-2 col-xs-4 im">
						<div class="item-metrica align-center">
							<i>Data Entregue</i><br/>
							<b class="tarefa-entrega">-</b>
						</div>
					</div>
					<div class="col col-sm-2 col-xs-4 im">
						<div class="item-metrica align-center">
							<i>Tempo Previsto</i><br/><b>60min</b>
						</div>
					</div>
					<div class="col col-sm-2 col-xs-4 im">
						<div class="item-metrica align-center">
							<i>Tempo Esforço</i><br/>
							<b class="tarefa-totalesforco">20</b><b>min</b>
						</div>
					</div>
					<div class="col col-sm-2 col-xs-4 im">
						<div class="item-metrica align-center">
							<i>Núm. Andamentos</i><br/>
							<b class="tarefa-qtandamentos">2</b>
						</div>
					</div>
					<div class="col col-sm-2 col-xs-4 im">
						<div class="item-metrica align-center">
							<i>Situação</i><br/>
							<b class="tarefa-situacao">Pendente</b>
						</div>
					</div>
				</div>
			</div><!-- fim .container -->
			<div class="container conteudo">
				<div class="row">
					<div class="col col-sm-12 col-titulo">Detalhes da tarefa</div>
					<div class="col col-sm-2 col-xs-3"><label>Projeto:</label><br/><a href="#">1234</a></div>
					<div class="col col-sm-2 col-xs-3"><label>Tarefa:</label><br/>1201</div>
					<div class="col col-sm-8 col-xs-6">
						<label>Equipe:</label><br/>
						<select name="tarefa-equipe" class="form-control hidden">
							<option>Front-end</option>
							<option>Back-end</option>
							<option>Atendimento</option>
						</select>
						<div class="tarefa-equipe">Front-end</div>
					</div>
					<div class="col col-sm-12 col-xs-12">
						<label>Tarefa:</label><br/>
						<input name="tarefa-titulo" type="text" class="form-control hidden">
						<div class="tarefa-titulo">Levantamento de requisitos</div>
					</div>
					<div class="col col-sm-12 col-xs-12">
						<label>Descrição:</label><br/>
						<textarea name="tarefa-descricao" class="form-control hidden" rows="5"></textarea>
						<div class="tarefa-descricao">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eu dui felis. Integer sollicitudin dapibus ipsum at malesuada. Nulla vel vehicula tellus. Aliquam at sem a magna pharetra sagittis sit amet quis mauris. Suspendisse euismod molestie ultrices. In a ligula eu lacus convallis posuere sit amet eu ligula. Fusce et augue eu urna commodo tincidunt quis a diam. In fringilla convallis lobortis. In non ex vel nibh maximus dapibus. In egestas nisl et nisl consequat, at faucibus risus sollicitudin. Aliquam erat volutpat. In vehicula libero id ante iaculis laoreet.</div>
					</div>
				</div><!-- fim .row -->
				<div class="row">
					<div class="col col-sm-12 col-botoes align-right botoes">
						<button name="tarefa-cancelar" class="btn btn-default hidden">Cancelar</button>
						<button name="tarefa-salvar" class="btn btn-info hidden">Salvar</button>
						<button name="tarefa-editar" class="btn btn-default">Editar</button>
						<button name="tarefa-entregar" class="btn btn-info">Entregar</button>
					</div><!-- fim .col .botoes -->
				</div><!-- fim .row -->
			</div><!-- fim .container .conteudo -->
			<div class="container conteudo">
				<div class="row">
					<div class="col col-sm-12 col-titulo">Andamentos</div><!-- fim .col .col-titulo -->
				</div><!-- fim .row -->
				<div class="row inputs-andamento">
					<div class="col col-sm-12">
						<label>O que foi feito?</label><br/>
						<textarea name="andamento-descricao" class="form-control" placeholder="Descrição*" rows="3"></textarea>
					</div>
					<div class="col col-sm-4 col-xs-6 p4r">
						<select name="andamento-situacao" class="form-control placeholder">
							<option value ="" selected>Situação</option>
							<option>Trabalhando</option>
							<option>Pendente</option>
							<option>Concluído</option>
						</select>
					</div>
					<div class="col col-sm-2 col-xs-6">
						<div class="input-group">
							<input name="andamento-esforco" type="text" class="form-control time somenteNumero" placeholder="Min*">
							<span class="input-group-btn">
								<button class="btn btn-default time" type="button">
									<span class="glyphicon glyphicon-play"></span>
								</button>
							</span>
						</div><!-- /input-group -->
					</div>
					<div class="col col-sm-6 col-xs-12">
						<button name="andamento-salvar" class="btn btn-info float-right">Salvar</button>
					</div>
				</div><!-- fim .row -->
				<div class="row lista-andamentos">
					<div class="col col-sm-12 col-andamento">
						<label><a href="#">Renê Felipe Silva</a></label><br/>
						<b>Pendente</b><i> - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eu dui felis. Integer sollicitudin dapibus ipsum at malesuada. 
							Nulla vel vehicula tellus. Aliquam at sem a magna pharetra sagittis sit amet quis mauris. Suspendisse euismod molestie ultrices. 
							In a ligula eu lacus convallis posuere sit amet eu ligula.</i><br/>
						<label>13/10/2017 às 13:04:51</label><label class="float-right">Esforço: 10min</label>
					</div><!-- .col-sm-12 .col-andamento -->
					<div class="col col-sm-12 col-andamento">
						<label><a href="#">Matheus Marcos Frota</a></label><br/>
						<b>Trabalhando</b><i> - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eu dui felis. Integer sollicitudin dapibus ipsum at malesuada. 
							Nulla vel vehicula tellus. Aliquam at sem a magna pharetra sagittis sit amet quis mauris. Suspendisse euismod molestie ultrices. 
							In a ligula eu lacus convallis posuere sit amet eu ligula.</i><br/>
						<label>11/10/2017 às 09:34:02</label><label class="float-right">Esforço: 28min</label>
					</div><!-- .col-sm-12 .col-andamento -->	
				</div><!-- fim .row -->
			</div><!-- fim .container .conteudo -->
		</main>

<?php include_once 'footer.php' ?>

	<script>
		/*INICIO CRONOMETRO*/
		var clickCronometro = 0;
		var timeCronometro;
		var ponteiroRelogio = 0;
		var intervaloConometro;
		var intervaloPonteiroRelogio;
		$("button.time").click(function(){
			timeCronometro = $("input[name='andamento-esforco']").val();
			if(timeCronometro==""){
				timeCronometro = 0;
			}
			clickCronometro++;
			$("input[name='andamento-esforco']").val(timeCronometro);
			if(clickCronometro % 2 == 0){
				$('button.time').removeClass('btn-info');
				$('button.time').addClass('btn-default');
				$("button.time>span").removeClass('glyphicon-pause');
				$("button.time>span").addClass('glyphicon-play');
				/*pauso o cronometro*/
				clearInterval(intervaloCronometro);
			}else{
				$('button.time').removeClass('btn-default');
				$('button.time').addClass('btn-info');
				$("button.time>span").removeClass('glyphicon-play');
				$("button.time>span").addClass('glyphicon-pause');
				/*inicia o cronometro*/
				intervaloCronometro = setInterval(function() {
					timeCronometro++;
					$("input[name='andamento-esforco']").val(timeCronometro);
				},60000);
			}
		});
		/*FIM CRONOMETRO*/
		
		/* ADD ANDAMENTO */
		$("button[name='andamento-salvar']").click(function(){
			var qtandamentos = $('.tarefa-qtandamentos').text();
			var totalesforco = $('.tarefa-totalesforco').text();			
			var autor = 'Bruno Modinuti';
			var descricao = $("textarea[name='andamento-descricao']").val();
			var situacao = $("select[name='andamento-situacao']").val();
			var esforco = $("input[name='andamento-esforco']").val();
			var data = new Date();
			var dia = data.getDate();
			var mes = data.getMonth();
			var ano = data.getFullYear();
			var hora = data.getHours();
			var minuto = data.getMinutes();
			var segundo = data.getSeconds();
			/*ADICIONA 0 EM NUMEROS MENORES QUE 10*/
			if(dia<=9){dia='0'+dia;}
			if(mes<=9){mes='0'+mes}
			if(hora<=9){hora='0'+hora}
			if(minuto<=9){minuto='0'+minuto}
			if(segundo<=9){segundo='0'+segundo;}
			/*CONCATENA STRING*/
			var str_data = dia+'/'+mes+'/'+ano;
			var str_hora = hora+':'+minuto+':'+segundo;
			/*HTML QUE SERA ADICIONADO NA PÁGINA*/
			var html=
				'<div class="col col-sm-12 col-andamento animated fadeIn">'+
					'<label><a href="#">'+autor+'</a></label><br/>'+
					'<b>'+situacao+'</b><i> - '+descricao+'</i><br/>'+
					'<label>'+str_data+' às '+str_hora+'</label>'+
					'<label class="float-right">Esforço: '+esforco+'min</label>'+
				'</div>';
			if(descricao!=""&&esforco!=""&&esforco>"0"&&situacao!=""){
				qtandamentos++;
				totalesforco=parseInt(totalesforco)+parseInt(esforco);
				$('.tarefa-totalesforco').text(totalesforco.toString());
				$('.tarefa-situacao').text(situacao);
				$('.tarefa-qtandamentos').text(qtandamentos);
				if(clickCronometro % 2 != 0){$('button.time').click();}
				/*REMOVE CLASSE REQUIRED*/
				$("textarea[name='andamento-descricao']").removeClass("required");
				$(".time").removeClass("required");
				$("select[name='andamento-situacao']").addClass("placeholder");
				/*ESCREVE O HTML NA PÁGINA*/
				$("div.lista-andamentos").prepend(html);
				/*LIMPA INPUTS*/
				$("textarea[name='andamento-descricao']").val('');
				$("select[name='andamento-situacao']").val('');
				$("input[name='andamento-esforco']").val('');
			}else{
				if(descricao==""){
					$("textarea[name='andamento-descricao']").addClass("required");
				}else{
					$("textarea[name='andamento-descricao']").removeClass("required");
				}if(situacao==""){
					$("select[name='andamento-situacao").addClass("required");
				}else{
					$("select[name='andamento-situacao").removeClass("required");
				}
				if(esforco==""){
					$(".time").addClass("required");
				}else{
					if(esforco=="0"){
						$(".time").addClass("required");
					}else{
						$(".time").removeClass("required");
					}
				}
			}			
		});
		/* FIM ADD ANDAMENTO */

		/*tarefa início*/
		$("button[name='tarefa-editar']").click(
			function(){
				console.log('Editar');
				/*get*/
				var titulo = $('.tarefa-titulo').text();
				var descricao = $('.tarefa-descricao').text();
				var previsao = $('.tarefa-previsao').text();
				/*set*/
				$("input[name='tarefa-titulo']").val(titulo);
				$("textarea[name='tarefa-descricao']").text(descricao);
				$("input[name='tarefa-previsao']").val(previsao);
				/*remove hidden*/
				$("input[name='tarefa-titulo']").removeClass('hidden');
				$("textarea[name='tarefa-descricao']").removeClass('hidden');
				$("input[name='tarefa-previsao']").removeClass('hidden');
				$("select[name='tarefa-equipe']").removeClass('hidden');
				$("button[name='tarefa-cancelar']").removeClass('hidden');
				$("button[name='tarefa-salvar']").removeClass('hidden');
				/*adiciona hidden*/
				$(".tarefa-titulo").addClass('hidden');
				$(".tarefa-descricao").addClass('hidden');
				$(".tarefa-previsao").addClass('hidden');
				$(".tarefa-equipe").addClass('hidden');
				$("button[name='tarefa-editar']").addClass('hidden');
				$("button[name='tarefa-entregar']").addClass('hidden');
			});
		$("button[name='tarefa-cancelar']").click(
			function(){
				console.log('Cancelar');
				/*remover hidden*/
				$(".tarefa-titulo").removeClass('hidden');
				$(".tarefa-descricao").removeClass('hidden');
				$(".tarefa-previsao").removeClass('hidden');
				$(".tarefa-equipe").removeClass('hidden');
				$("button[name='tarefa-editar']").removeClass('hidden');
				$("button[name='tarefa-entregar']").removeClass('hidden');
				/*adicionar hidden*/
				$("input[name='tarefa-titulo']").addClass('hidden');
				$("textarea[name='tarefa-descricao']").addClass('hidden');
				$("input[name='tarefa-previsao']").addClass('hidden');
				$("select[name='tarefa-equipe']").addClass('hidden');
				$("button[name='tarefa-cancelar").addClass('hidden');
				$("button[name='tarefa-salvar").addClass('hidden');	
			});
		$("button[name='tarefa-salvar']").click(
			function(){
				console.log('Salvar');
				/*get*/
				var titulo = $("input[name='tarefa-titulo']").val();
				var descricao = $("textarea[name='tarefa-descricao']").val();
				var previsao = $("input[name='tarefa-previsao']").val();
				var equipe = $("select[name='tarefa-equipe']").val();
				/*set*/
				$(".tarefa-titulo").text(titulo);
				$(".tarefa-descricao").text(descricao);
				$(".tarefa-previsao").text(previsao);
				$(".tarefa-equipe").text(equipe);
				/*remover hidden*/
				$(".tarefa-titulo").removeClass('hidden');
				$(".tarefa-descricao").removeClass('hidden');
				$(".tarefa-previsao").removeClass('hidden');
				$(".tarefa-equipe").removeClass('hidden');
				$("button[name='tarefa-editar']").removeClass('hidden');
				$("button[name='tarefa-entregar']").removeClass('hidden');
				/*adicionar hidden*/
				$("input[name='tarefa-titulo']").addClass('hidden');
				$("textarea[name='tarefa-descricao']").addClass('hidden');
				$("input[name='tarefa-previsao']").addClass('hidden');
				$("select[name='tarefa-equipe']").addClass('hidden');
				$("button[name='tarefa-cancelar").addClass('hidden');
				$("button[name='tarefa-salvar").addClass('hidden');	
			});
		$('button[name="tarefa-entregar"]').click(
			function(){
				console.log('Entregar');
				var data = new Date();
				var dia = data.getDate();
				var mes = data.getMonth();
				var ano = data.getFullYear();
				var hora = data.getHours();
				var minuto = data.getMinutes();
				var segundo = data.getSeconds();
				/*ADICIONA 0 EM NUMEROS MENORES QUE 10*/
				if(dia<=9){dia='0'+dia;}
				if(mes<=9){mes='0'+mes}
				if(hora<=9){hora='0'+hora}
				if(minuto<=9){minuto='0'+minuto}
				if(segundo<=9){segundo='0'+segundo;}
				/*CONCATENA STRING*/
				var str_data = dia+'/'+mes+'/'+ano;
				var str_hora = hora+':'+minuto+':'+segundo;
				/*get*/
				var situacao = $('.tarefa-situacao').text();
				/**/
				var sucesso = 
					'<div class="row andamento-concluido">'+
						'<div class="col col-sm-12 tarefa-entregue animated bounceIn">'+
							'<h1><b class="glyphicon glyphicon-ok"></b> Tarefa entregue</h1><i>'+str_data+' às '+str_hora+'</i>'+
						'</div>'+
					'</div>';

				var alert_sucesso =
					'<div class="col col-sm-12 im msg">'+
						'<div class="alert alert-success animated bounce" role="alert">'+
 							'<strong>Muito bom!</strong> Tarefa entregue com sucesso.'+
						'</div>'+
					'</div>';
				var alert_erro = 
					'<div class="col col-sm-12 im msg">'+
						'<div class="alert alert-danger animated bounce" role="alert">'+
 							'<strong>Alerta!</strong> Não é possível realizar esta operação, a tarefa não foi concluída.'+
						'</div>'+
					'</div>';
				if(situacao=="Concluído"){
					$('.tarefa-entrega').text(str_data);
					$('.inputs-andamento,.botoes').remove();
					$('.lista-andamentos').prepend(sucesso);
					$('.row.im').prepend(alert_sucesso);
					setTimeout(
						function(){
							$('.alert').removeClass('bounce');
							$('.alert').addClass('bounceOut');
						},5000
					);
					setTimeout(
						function(){
							$('.msg').remove();
						},5800
					);
				}else{
					$('.row.im').prepend(alert_erro);
					setTimeout(
						function(){
							$('.alert').removeClass('bounce');
							$('.alert').addClass('bounceOut');
						},5000
					);
					setTimeout(
						function(){
							$('.msg').remove();
						},5800
					);
				}
			});
		/*tarefa fim*/
	</script>
</html>
<?php include_once 'header.php' ?>

		<main class="animated fadeIn">
			<div class="container bc">
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url('inicio')?>">Início</a></li>
					<li><a href="<?php echo base_url('projetos')?>">Projetos</a></li>
					<li class="active"><?= $projeto->id_projeto ?></li>
				</ol>
			</div>
			<div class="container">
				<div class="row im">
					<div class="col col-sm-2 col-xs-4 im">
						<div class="item-metrica align-center">
							<i>Data Prevista</i><br/><b><?= $projeto->dt_prevista ?></b>
						</div>
					</div>
					<div class="col col-sm-2 col-xs-4 im">
						<div class="item-metrica align-center">
							<i>Data Entregue</i><br/>
							<b class="projeto-entrega">-</b>
						</div>
					</div>
					<div class="col col-sm-2 col-xs-4 im">
						<div class="item-metrica align-center">
							<i>Tempo Previsto</i><br/>
							<b>0min</b>
						</div>
					</div>
					<div class="col col-sm-2 col-xs-4 im">
						<div class="item-metrica align-center">
							<i>Tempo Esforço</i><br/>
							<b>0min</b>
						</div>
					</div>
					<div class="col col-sm-2 col-xs-4 im">
						<div class="item-metrica align-center">
							<i>Concluído</i><br/><b>0%</b>
						</div>
					</div>
					<div class="col col-sm-2 col-xs-4 im">
						<div class="item-metrica align-center">
							<i>Situação</i><br/><b class="projeto-situacao">Pendente</b>
						</div>
					</div>
				</div>
			</div><!-- fim .container -->
			<div class="container conteudo">
				<div class="row">
					<div class="col col-sm-12 col-titulo">Detalhes do projeto</div>
					<div class="col col-sm-2 col-xs-3"><label>Orçamento:</label><br/><a href="<?php echo base_url('orcamentos/'.$projeto->fk_orcamento)?>"><?= $projeto->fk_orcamento ?></a></div>
					<div class="col col-sm-2 col-xs-3"><label>Projeto:</label><br/><?= $projeto->id_projeto ?></div>
					<div class="col col-sm-8">
						<label>Cliente:</label><br/><a href="#" title="Mostra tudo sobre o cliente"><?= $projeto->nome ?></a>
					</div>
					<div class="col col-sm-12 col-xs-12">
						<label>Projeto:</label><br/>
						<input name="projeto-titulo" type="text" class="form-control hidden">
						<div class="projeto-titulo"><?= $projeto->titulo ?></div>
					</div>
					<div class="col col-sm-12 col-xs-12">
						<label>Descrição:</label><br/>
						<textarea name="projeto-descricao" class="form-control hidden" rows="5"></textarea>
						<div class="projeto-descricao"><?= $projeto->descricao ?></div>
					</div>
				</div><!-- fim .row -->
				<div class="row">
					<div class="col col-sm-12 col-botoes align-right botoes">
						<button name="projeto-cancelar" class="btn btn-default hidden">Cancelar</button>
						<button name="projeto-salvar" class="btn btn-info hidden">Salvar</button>
						<button name="projeto-editar" class="btn btn-default">Editar</button>
						<button name="projeto-entregar" class="btn btn-info">Entregar</button>
					</div>
				</div><!-- fim .row -->
			</div><!-- fim .container .conteudo -->
			<div class="container conteudo">
				<div class="row">
					<div class="col col-sm-1 col-xs-2 col-titulo">Código</div>
					<div class="col col-sm-7 col-xs-7 col-titulo">Tarefa</div>
					<div class="col col-sm-2 hidden-xs col-titulo">Equipe</div>
					<div class="col col-sm-2 col-xs-3 col-titulo">Situação</div>
				</div><!-- fim .row -->
				<?php
					$contador=0;
					foreach ($tarefas as $i) {
						echo '
						<a href="#" class="link-list">
							<div class="row">
								<div class="col col-sm-1 col-xs-2">'.$i->id_tarefa.'</div>
								<div class="col col-sm-7 col-xs-7">'.$i->titulo.'</div>
								<div class="col col-sm-2 hidden-xs">'.$i->nome.'</div>
								<div class="col col-sm-2 col-xs-3">Pendente</div>
							</div>
						</a>';
						$contador++;
					}
					if($contador == 0){echo '<h2>Nenhum cliente encontrado<h2>';}
				?>
			</div>
		</main>

<?php include_once 'footer.php' ?>
	
	<script>
		$("button[name='projeto-editar']").click(
			function(){
				console.log('Editar');
				/*get*/
				var titulo = $(".projeto-titulo").text();
				var descricao = $(".projeto-descricao").text();
				/*set*/
				$("input[name='projeto-titulo']").val(titulo);
				$("textarea[name='projeto-descricao']").text(descricao);
				/*remove hidden*/
				$("input[name='projeto-titulo']").removeClass('hidden');
				$("textarea[name='projeto-descricao']").removeClass('hidden');
				$("button[name='projeto-cancelar']").removeClass('hidden');
				$("button[name='projeto-salvar']").removeClass('hidden');
				/*adiciona hidden*/
				$(".projeto-titulo").addClass('hidden');
				$(".projeto-descricao").addClass('hidden')
				$("button[name='projeto-editar']").addClass('hidden');
				$("button[name='projeto-entregar']").addClass('hidden');;
			});
		$("button[name='projeto-cancelar']").click(
			function(){
				console.log('Cancelar');
				/*remover hidden*/
				$(".projeto-titulo").removeClass('hidden');
				$(".projeto-descricao").removeClass('hidden');
				$("button[name='projeto-editar']").removeClass('hidden');
				$("button[name='projeto-entregar']").removeClass('hidden');
				/*adiciona hidden*/
				$("input[name='projeto-titulo']").addClass('hidden');
				$("textarea[name='projeto-descricao']").addClass('hidden');
				$("button[name='projeto-cancelar']").addClass('hidden');
				$("button[name='projeto-salvar']").addClass('hidden');
			});
		$("button[name='projeto-salvar']").click(
			function(){
				console.log('Salvar');
				/*get*/
				var titulo = $("input[name='projeto-titulo']").val();
				var descricao = $("textarea[name='projeto-descricao']").val();
				/*set*/
				$(".projeto-titulo").text(titulo);
				$(".projeto-descricao").text(descricao);
				/*remove hidden*/
				$(".projeto-titulo").removeClass('hidden');
				$(".projeto-descricao").removeClass('hidden');
				$("button[name='projeto-editar']").removeClass('hidden');
				$("button[name='projeto-entregar']").removeClass('hidden');
				/*adiciona hidden*/
				$("input[name='projeto-titulo']").addClass('hidden');
				$("textarea[name='projeto-descricao']").addClass('hidden');
				$("button[name='projeto-cancelar']").addClass('hidden');
				$("button[name='projeto-salvar']").addClass('hidden');
			});
		$("button[name='projeto-entregar']").click(function(){
			console.log('Entregar');

			var data = new Date();
			var dia = data.getDate();
			var mes = data.getMonth();
			var ano = data.getFullYear();
			var hora = data.getHours();
			var minuto = data.getMinutes();
			var segundo = data.getSeconds();
			/*ADICIONA 0 EM NUMEROS MENORES QUE 10*/
			if(dia<=9){dia='0'+dia;}
			if(mes<=9){mes='0'+mes}
			if(hora<=9){hora='0'+hora}
			if(minuto<=9){minuto='0'+minuto}
			if(segundo<=9){segundo='0'+segundo;}
			/*CONCATENA STRING*/
			var str_data = dia+'/'+mes+'/'+ano;
			var str_hora = hora+':'+minuto+':'+segundo;

			/*get*/
			var situacao = $(".projeto-situacao").text();

			var alert_erro =
				'<div class="col col-sm-12 im msg">'+
					'<div class="alert alert-danger animated bounce" role="alert">'+
							'<strong>Alerta!</strong> Não é possível realizar esta operação, o projeto possui tarefas pendentes.'+
					'</div>'+
				'</div>';
			var alert_sucesso =
				'<div class="col col-sm-12 im msg">'+
					'<div class="alert alert-success animated bounce" role="alert">'+
							'<strong>Muito bom!</strong> Projeto entregue com sucesso.'+
					'</div>'+
				'</div>';

			if(situacao=="Concluído"){
				$(".projeto-entrega").text(str_data);
				$(".botoes").remove();
				$('.row.im').prepend(alert_sucesso);
				setTimeout(
					function(){
						$('.alert').removeClass('bounce');
						$('.alert').addClass('bounceOut');
					},5000
				);
				setTimeout(
					function(){
						$('.msg').remove();
					},5800
				);
			}else{
				$('.row.im').prepend(alert_erro);
				setTimeout(
					function(){
						$('.alert').removeClass('bounce');
						$('.alert').addClass('bounceOut');
					},5000
				);
				setTimeout(
					function(){
						$('.msg').remove();
					},5800
				);
			}
		});
	</script>
</html>
<?php $this->load->view('header') ?>
		<main class="animated fadeIn">
			<div class="container bc">
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url('inicio')?>">Início</a></li>
					<li><a href="<?php echo base_url('clientes')?>">Clientes</a></li>
					<li class="active"><?php echo empty($cliente->id_cliente)?'Novo':$cliente->id_cliente;?></li>
				</ol>
			</div>
			<div class="mensagens container"></div>
			<div class="container conteudo">
				<form id="form-cliente">
					<div class="row row-input">
						<div class="col col-xs-12">
							<label>Dados Pessoais</label>
						</div>
						<div class="col col-sm-8 col-xs-12 col-input">
							<input type="text" name="id" value="<?php echo empty($cliente->id_cliente)?'':$cliente->id_cliente;?>" hidden>
							<input type="text" name="nome" class="form-control req" 
							placeholder="Nome completo*" maxlength="100" 
							value="<?php echo empty($cliente->nome)?'':$cliente->nome;?>">
						</div>
						<div class="col col-sm-4 col-xs-12 col-input">
							<select name="tipopessoa" class="form-control">
								<option value="PF" selected>Pessoa Física</option>
								<option value="PF" <?= (isset($cliente->tp_pessoa) && $cliente->tp_pessoa === 'PF' ? 'selected' : '') ?> >Pessoa Física</option>
								<option value="PJ" <?= (isset($cliente->tp_pessoa) && $cliente->tp_pessoa === 'PJ' ? 'selected' : '') ?> >Pessoa Jurídica</option>
							</select>
						</div>
						<div class="col col-sm-6 col-xs-6 pj hidden col-input">
							<input type="text" name="nome_fantasia" class="form-control req" 
							placeholder="Nome fantasia*" maxlength="100"
							value="<?php echo empty($cliente->nome_fantasia)?'':$cliente->nome_fantasia;?>">
						</div>
						<div class="col col-sm-6 col-xs-6 pj hidden col-input">
							<input type="text" name="razaosocial" class="form-control req" 
							placeholder="Razão social*" maxlength="100"
							value="<?php echo empty($cliente->razao_social)?'':$cliente->razao_social;?>">
						</div>
						<div class="col col-sm-5 col-xs-6 pf col-input">
							<input type="text" name="cpf" class="form-control maskCPF req validar" 
							placeholder="CPF*" maxlength="20" 
							value="<?php echo empty($cliente->cpf)?'':$cliente->cpf;?>">
						</div>
						<div class="col col-sm-6 col-xs-6 pj hidden col-input">
							<input type="text" name="cnpj" class="form-control maskCNPJ req" 
							placeholder="CNPJ*" maxlength="20"
							value="<?php echo empty($cliente->cnpj)?'':$cliente->cnpj;?>">
						</div>
						<div class="col col-sm-5 col-xs-6 pf col-input">
							<input name="rg" type="text" class="form-control" 
							placeholder="RG" maxlength="20" 
							value="<?php echo empty($cliente->rg)?'':$cliente->rg;?>">
						</div>
						<div class="col col-sm-6 col-xs-6 pj hidden col-input">
							<input name="ie" type="text" class="form-control" 
							placeholder="Inscrição estadual" maxlength="20"
							value="<?php echo empty($cliente->ie)?'':$cliente->ie;?>">
						</div>
						<div class="col col-sm-2 col-xs-6 pf col-input">
							<input type="text" name="dt_nascimento" class="form-control data maskDATA" 
							placeholder="Nascimento" maxlength="10" 
							value="<?php echo empty($cliente->dt_nascimento)?'':$cliente->dt_nascimento;?>">
						</div>
					</div>
					<!-- CONTATOS -->
					<div class="row row-input">
						<div class="col col-sm-12 col-xs-12">
							<label>Contatos</label>
						</div>
						<div class="col col-sm-6 col-xs-12 col-input email">
							<input type="text" name="email" class="form-control req" 
							placeholder="E-mail*" maxlength="100" 
							value="<?php echo empty($cliente->email)?'':$cliente->email;?>">
						</div>
					</div>
					<div class="row row-input">
						<div class="col col-sm-6 col-xs-6 col-input">
							<input type="text" name="tel_residencial" class="form-control maskTel" 
							placeholder="Telefone residencial" maxlength="15" 
							value="<?php echo empty($cliente->tel_residencial)?'':$cliente->tel_residencial;?>">
						</div>
						<div class="col col-sm-6 col-xs-6 col-input">
							<input type="text" name="tel_recado" class="form-control maskTel" 
							placeholder="Telefone recado" maxlength="15" 
							value="<?php echo empty($cliente->tel_recado)?'':$cliente->tel_recado;?>">
						</div>
						<div class="col col-sm-4 col-xs-6 col-input">
							<input type="text" name="tel_comercial" class="form-control maskTel" 
							placeholder="Telefone comercial" maxlength="15" 
							value="<?php echo empty($cliente->tel_comercial)?'':$cliente->tel_comercial;?>">
						</div>
						<div class="col col-sm-2 col-xs-6 col-input">
							<input type="number" name="ramal" class="form-control" 
							placeholder="Ramal" maxlength="6" 
							value="<?php echo empty($cliente->tel_comercial_ramal)?'':$cliente->tel_comercial_ramal;?>">
						</div>
						<div class="col col-sm-6 col-xs-6 col-input">
							<input type="text" name="tel_celular" class="form-control maskCel" 
							placeholder="Telefone celular" maxlength="15" 
							value="<?php echo empty($cliente->tel_celular)?'':$cliente->tel_celular;?>">
						</div>
					</div>
					<!-- ENDEREÇO -->
					<div class="row row-input">
						<div class="col col-xs-12">
							<label>Endereço</label>
						</div>
						<div class="col col-sm-3 col-xs-6 col-input">
							<input name="cep" type="text" class="form-control maskCEP" 
							placeholder="CEP" maxlength="9" 
							value="<?php echo empty($cliente->cep)?'':$cliente->cep;?>">
						</div>
						<div class="col col-sm-7 col-xs-12 col-input">
							<input name="endereco" type="text" class="form-control" 
							placeholder="Endereço" maxlength="100" 
							value="<?php echo empty($cliente->endereco)?'':$cliente->endereco;?>">
						</div>
						<div class="col col-sm-2 col-xs-6 col-input">
							<input name="numero" type="number" class="form-control" 
							placeholder="Número" maxlength="6" 
							value="<?php echo empty($cliente->numero)?'':$cliente->numero;?>">
						</div>
						<div class="col col-sm-3 col-xs-6 col-input">
							<input name="bairro" type="text" class="form-control" 
							placeholder="Bairro" maxlength="50" 
							value="<?php echo empty($cliente->bairro)?'':$cliente->bairro;?>">
						</div>
						<div class="col col-sm-9 col-xs-12 col-input">
							<input name="complemento" type="text" class="form-control" 
							placeholder="Complemento" maxlength="100" 
							value="<?php echo empty($cliente->complemento)?'':$cliente->complemento;?>"> 
						</div>
						<div class="col col-sm-3 col-xs-6 col-input">
							<select name="estado" class="form-control placeholder req">
								<option value="" selected>UF*</option>
								<option value="AC" <?= (isset($cliente->uf) && $cliente->uf === 'AC' ? 'selected' : '') ?> >Acre</option>
								<option value="AL" <?= (isset($cliente->uf) && $cliente->uf === 'AL' ? 'selected' : '') ?> >Alagoas</option>
								<option value="AP" <?= (isset($cliente->uf) && $cliente->uf === 'AP' ? 'selected' : '') ?> >Amapá</option>
								<option value="AM" <?= (isset($cliente->uf) && $cliente->uf === 'AM' ? 'selected' : '') ?> >Amazonas</option>
								<option value="BA" <?= (isset($cliente->uf) && $cliente->uf === 'BA' ? 'selected' : '') ?> >Bahia</option>
								<option value="CE" <?= (isset($cliente->uf) && $cliente->uf === 'CE' ? 'selected' : '') ?> >Ceará</option>
								<option value="DF" <?= (isset($cliente->uf) && $cliente->uf === 'DF' ? 'selected' : '') ?> >Distrito Federal</option>
								<option value="ES" <?= (isset($cliente->uf) && $cliente->uf === 'ES' ? 'selected' : '') ?> >Espirito Santo</option>
								<option value="GO" <?= (isset($cliente->uf) && $cliente->uf === 'GO' ? 'selected' : '') ?> >Goiás</option>
								<option value="MA" <?= (isset($cliente->uf) && $cliente->uf === 'MA' ? 'selected' : '') ?> >Maranhão</option>
								<option value="MT" <?= (isset($cliente->uf) && $cliente->uf === 'MT' ? 'selected' : '') ?> >Mato Grasso</option>
								<option value="MS" <?= (isset($cliente->uf) && $cliente->uf === 'MS' ? 'selected' : '') ?> >Mato Grassa do Sul</option>
								<option value="MG" <?= (isset($cliente->uf) && $cliente->uf === 'MG' ? 'selected' : '') ?> >Minas Geais</option>
								<option value="PA" <?= (isset($cliente->uf) && $cliente->uf === 'PA' ? 'selected' : '') ?> >Pará</option>
								<option value="PB" <?= (isset($cliente->uf) && $cliente->uf === 'PB' ? 'selected' : '') ?> >Paraíba</option>
								<option value="PR" <?= (isset($cliente->uf) && $cliente->uf === 'PR' ? 'selected' : '') ?> >Paraná</option>
								<option value="PE" <?= (isset($cliente->uf) && $cliente->uf === 'PE' ? 'selected' : '') ?> >Pernambuco</option>
								<option value="PI" <?= (isset($cliente->uf) && $cliente->uf === 'PI' ? 'selected' : '') ?> >Piauí</option>
								<option value="RJ" <?= (isset($cliente->uf) && $cliente->uf === 'RJ' ? 'selected' : '') ?> >Rio de Janeiro</option>
								<option value="RN" <?= (isset($cliente->uf) && $cliente->uf === 'RN' ? 'selected' : '') ?> >Rio Grande do Norte</option>
								<option value="RS" <?= (isset($cliente->uf) && $cliente->uf === 'RS' ? 'selected' : '') ?> >Rio Grande do Sul</option>
								<option value="RO" <?= (isset($cliente->uf) && $cliente->uf === 'RO' ? 'selected' : '') ?> >Rondônia</option>
								<option value="RR" <?= (isset($cliente->uf) && $cliente->uf === 'RR' ? 'selected' : '') ?> >Roraima</option>
								<option value="SC" <?= (isset($cliente->uf) && $cliente->uf === 'SC' ? 'selected' : '') ?> >Santa Catarina</option>
								<option value="SP" <?= (isset($cliente->uf) && $cliente->uf === 'SP' ? 'selected' : '') ?> >São Paulo</option>
								<option value="SE" <?= (isset($cliente->uf) && $cliente->uf === 'SE' ? 'selected' : '') ?> >Sergipe</option>
								<option value="TO" <?= (isset($cliente->uf) && $cliente->uf === 'TO' ? 'selected' : '') ?> >Tocantins</option>
							</select>
						</div>
						<div class="col col-sm-3 col-xs-6 col-input">
							<select name="cidade" class="form-control placeholder req">
								<option value="" selected>Cidade*</option>
								<?php
									foreach ($cidades as $cidade) {
										if(isset($cliente->fk_cidade) && $cidade->id_cidade==$cliente->fk_cidade){
											echo '<option value="'.$cidade->id_cidade.'" data-uf="'.$cidade->uf.'" selected>'.$cidade->nome.'</option>';
										}else{
											echo '<option value="'.$cidade->id_cidade.'" data-uf="'.$cidade->uf.'">'.$cidade->nome.'</option>';
										}
									}
								?>
							</select>
						</div>
					</div>
					<!--
					<div class="row row-input">
						<div class="col col-xs-12">
							<label>Dados do Usuário*</label>
						</div>

						<div class="col col-sm-3 col-xs-6 col-input">
							<select name="tipousuario" class="form-control">
								<option value="1" selected>Cliente</option>
								<option value="1">Cliente</option>
								<option value="2">Profissional</option>
								<option value="3">Administrador</option>
							</select>
						</div>
						<div class="col col-sm-3 col-xs-6 col-input equipe hidden">
							<select name="equipe" class="form-control placeholder req" required="">
								<option value="" selected>Equipe*</option>
								<option value="1">Front-end</option>
								<option value="2">Back-end</option>
								<option value="3">Qualidade</option>
								<option value="4">Atendimento</option>
							</select>
						</div>
						
						<div class="col col-sm-3 col-xs-6 col-input">
							<input type="password" name="senha1" class="form-control req" placeholder="Senha*" maxlength="100">
						</div>
						<div class="col col-sm-3 col-xs-6 col-input">
							<input type="password" name="senha2" class="form-control req" placeholder="Senha*" maxlength="100">
						</div>
						<div class="col col-sm-3 col-xs-12 col-input">
							<select class="form-control">
								<option value="2" selected>Desabilitado</option>
								<option value="1">Habilitado</option>
								<option value="2">Desabilitado</option>
							</select>
						</div>			
					</div>
				-->
				</form>
				<div class="row">
					<div class="col col-sm-12 col-botoes align-right botoes">
						<?php 
							if(isset($cliente->id_cliente)){
								echo '<button name="remover" class="btn btn-danger" data-toggle="modal" data-target="#ModalConfirmacao">Remover</button>
									<button name="atualizar" class="btn btn-info">Atualizar</button>';
							}else{
								echo '<button name="salvar" class="btn btn-info">Salvar</button>';
							}
						?>	
					</div>
				</div>
			</div>
		</main>

		<!-- Modal de confirmação -->
		<div class="modal fade" id="ModalConfirmacao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">Confirmação</h4>
		      </div>
		      <div class="modal-body">
		        Realmente deseja remover este cliente? Todos orçamentos e projetos serão perdidos.
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
		        <button type="button" name="confirmar" class="btn btn-danger">Sim</button>
		      </div>
		    </div>
		  </div>
		</div>
		<!-- Fim modal de confirmação -->
		
<?php $this->load->view('footer') ?>

	<script>
		/* Validação de cpf e cnpj inicio */
		var cpf_validado = false;
		var cnpj_validado = false;
		function verifica_cpf(){
			var cpf = $("input[name='cpf']").val();
			var cnpj = $("input[name='cnpj']").val();
			var required = $("input[name='cpf']").hasClass('required');
			if(cpf!=''){
				var teste = cpf_validate(cpf);
				if(teste==false){
					cpf_validado=false;
					alert_message(2);
					if(required==false){$("input[name='cpf']").toggleClass('required');}
				}else{
					cpf_validado=true;
					if(required==true){$("input[name='cpf']").toggleClass('required');}
				}
			}else{
				console.log('CPF não informado');
			}
		}
		function verifica_cnpj(){
			var cnpj = $("input[name='cnpj']").val();
			var required = $("input[name='cnpj']").hasClass('required');
			if(cnpj!=''){
				var teste = cnpj_validate(cnpj);
				if(teste==false){
					cnpj_validado=false;
					alert_message(3);
					if(required==false){$("input[name='cnpj']").toggleClass('required');}
				}else{
					cnpj_validado=true;
					if(required==true){$("input[name='cnpj']").toggleClass('required');}
				}
			}else{
				console.log('CNPJ não informado');
			}
		}
		$("input[name='cpf']").change(function(){
			verifica_cpf();
		});
		$("input[name='cnpj']").change(function(){
			verifica_cnpj();
		});
		/* Validação de cpf e cnpj fim */
		/* Insert */
		$("button[name='salvar']").click(function(){
			verifica_cpf();
			verifica_cnpj();
			var x = 0;
			$('.req').each(function(i,element){
				element = $(element);
				var expressao = element.is(":visible") && element.val() == "";
				element.toggleClass('required',expressao);
				if(expressao == true){
					x++;
				}
			});
			if(cpf_validado==false){
				$("input[name='cpf']").addClass('required');
			}else{
				$("input[name='cpf']").removeClass('required');
			}
			if(cnpj_validado==false){
				$("input[name='cnpj']").addClass('required');
			}else{
				$("input[name='cnpj']").removeClass('required');
			}
			if(x>0){
				alert_message(0);
			}else{
				if(cpf_validado==true || cnpj_validado==true){
					$("button[name='salvar']").attr('disabled','disabled');
					setTimeout(
					function(){
						window.location.replace('<?php echo base_url('clientes')?>');
					},5800);
					$.ajax({
						method: 'POST',
			    		url: '<?php echo base_url('welcome/cliente_insert')?>',
			    		data: $('#form-cliente').serialize(),
			    		dataType: 'json',
			    		success: function(data){
			    			if(data.status == true){
			    				alert_message(1);
			    			}
			    		},
			    		error: function(){
			    			alert_message(0);
			    		}
			    	});
				}
			}
		});
		/* Update */
		$("button[name='atualizar']").click(function(){
			verifica_cpf();
			verifica_cnpj();
			var x = 0;
			$('.req').each(function(i,element){
				element = $(element);
				var expressao = element.is(":visible") && element.val() == "";
				element.toggleClass('required',expressao);
				if(expressao == true){
					x++;
				}	
			});
			if(cpf_validado==false){
				$("input[name='cpf']").addClass('required');
			}else{
				$("input[name='cpf']").removeClass('required');
			}
			if(cnpj_validado==false){
				$("input[name='cnpj']").addClass('required');
			}else{
				$("input[name='cnpj']").removeClass('required');
			}
			if(x>0){
				alert_message(0);
			}else{
				if(cpf_validado==true || cnpj_validado==true){
					$("button[name='atualizar'],button[name='remover']").attr('disabled','disabled');
					setTimeout(
					function(){
						window.location.replace('<?php echo base_url('clientes')?>');
					},5800);
					$.ajax({
						method: 'POST',
			    		url: '<?php echo base_url('welcome/cliente_update')?>',
			    		data: $('#form-cliente').serialize(),
			    		dataType: 'json',
			    		success: function(data){
			    			if(data.status == true){
			    				alert_message(1);
			    			}
			    		},
			    		error: function(){
			    			alert_message(0);
			    		}
			    	});
			    }
			}	
		});
		/* Delete */
		$("button[name='confirmar']").click(function(){
			$.ajax({
				method: 'POST',
	    		url: '<?php echo base_url('welcome/cliente_delete')?>',
	    		data: $('#form-cliente').serialize(),
	    		dataType: 'json',
	    		success: function(data){
	    			if(data.status == true){
	    				$("button[name='atualizar'],button[name='remover']").attr('disabled','disabled');
	    				$('#ModalConfirmacao').modal('toggle');
	    				alert_message(1);
	    				setTimeout(
						function(){
							window.location.replace('<?php echo base_url('clientes')?>');
						},5800);
	    			}
	    		},
	    		error: function(){
	    			alert_message(0);
	    		}
	    	});
		});
	</script>
</html>
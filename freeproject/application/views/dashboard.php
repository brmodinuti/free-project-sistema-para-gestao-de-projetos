﻿<?php $this->load->view('header') ?>

		<main class="animated fadeIn">
			<div class="container bc">
				<ol class="breadcrumb">
					<li class="active">Início</li>
				</ol>
			</div>
			<div class="container">
				<div class="row gf">
					<div class="col col-sm-6 gf">
						<div class="grafico">
							<label>Orçamentos</label>
							<canvas class="gf-orcamentos"></canvas>
						</div>
					</div>
					<div class="col col-sm-6 gf">
						<div class="grafico">
							<label>Entrega dos Projetos</label>
							<canvas class="gf-projetos"></canvas>
						</div>
					</div>
				</div>
			</div>
		</main>

<?php $this->load->view('footer') ?>

	<script>
		/*ORÇAMENTOS*/
		var gf_orcamentos = document.getElementsByClassName("gf-orcamentos");
		var chartGraph = new Chart(gf_orcamentos,{
			type: 'line',
			data: {
				labels: ["Jan","Fev","Mar","Abr","Mai","Jun","Jul","Ago","Set","Out","Nov","Dez"],
				datasets: [
					{
						label: "Recusado",
						data: ["0","4","2","6","2","5","15","5","35","20","6","20"],
						borderWidth: 1,
						borderColor: '#ef0000',
						backgroundColor: '#fb5f5f'
					},
					{
						label: "Aprovado",
						data: ["0","5","5","6","5","10","30","25","70","50","50","90"],
						borderWidth: 1,
						borderColor: '#50c750',
						backgroundColor: '#5bfb90'
					},
					{
						label: "Pendente",
						data: ["0","10","10","13","11","20","60","50","90","70","66","110"],
						borderWidth: 1,
						borderColor: '#5cc0de',
						backgroundColor: '#77eeff'
					}
				]
			}
		});
		/*PROJETOS*/
		var gf_projetos = document.getElementsByClassName("gf-projetos");
		var chartGraph = new Chart(gf_projetos,{
			type: 'line',
			data: {
				labels: ["Jan","Fev","Mar","Abr","Mai","Jun","Jul","Ago","Set","Out","Nov","Dez"],
				datasets: [
					{
						label: "Atrasado",
						data: ["0","4","2","6","2","5","15","5","35","20","6","20"],
						borderWidth: 1,
						borderColor: '#ef0000',
						backgroundColor: '#fb5f5f'
					},
					{
						label: "Em dia",
						data: ["0","5","5","6","5","10","30","25","70","50","50","90"],
						borderWidth: 1,
						borderColor: '#50c750',
						backgroundColor: '#5bfb90'
					},
					{
						label: "Projetos",
						data: ["0","10","10","13","11","20","60","50","90","70","66","110"],
						borderWidth: 1,
						borderColor: '#5cc0de',
						backgroundColor: '#77eeff'
					}
				]
			}
		});
	</script>
</html>
drop database if exists freeproject;
create database freeproject;
use freeproject;

create table cidades(
	id_cidade int not null auto_increment,
	uf varchar(2) not null,
	nome varchar(60) not null,
	primary key (id_cidade)
);

insert into cidades(uf,nome)
	values  ('PR','Londrina'),
			('PR','Cambé'),
			('SP','São Paulo');

create table clientes (
	id_cliente int not null auto_increment, 
	tp_pessoa varchar(2),
    nome varchar(60),
    nome_fantasia varchar(60),
    razao_social varchar(60),
    cpf varchar(20),
    cnpj varchar(20),
    rg varchar(20),
    dt_nascimento varchar(10),
    ie varchar(20),
    tel_residencial varchar(20),
    tel_celular varchar(20),
    tel_comercial varchar(20),
    tel_comercial_ramal varchar(6),
    tel_recado varchar(20),
    cep varchar(20),
    endereco varchar(60),
    numero varchar(20),
    bairro varchar(20),
    complemento varchar(20),
    fk_cidade int,
    email varchar(60),
    primary key (id_cliente),
    foreign key (fk_cidade) references cidades (id_cidade)
);

insert into clientes(tp_pessoa, nome, cpf, cnpj, email, tel_celular, fk_cidade) 
	values  ('PF','Bruno Rafael de Andrade Modinuti','000.111.222-33','','brmodinuti@gmail.com', '(43) 99982-5204', 1);

create table equipes (
    id_equipe int not null auto_increment,
    nome varchar(60) not null,
    primary key (id_equipe)
);

insert into equipes(nome)
    values  ('Atendimento'),
            ('Teste'),
            ('Designer'),
            ('Front-end'),
            ('Back-end');

create table servicos (
    id_servico int not null auto_increment,
    nome varchar(45) not null,
    preco decimal(10,2) not null,
    fk_equipe int not null,
    primary key (id_servico),
    foreign key (fk_equipe) references equipes (id_equipe)
);

insert into servicos(nome, preco, fk_equipe)
    values  ('Treinamento','10.90', 1),
            ('Desenvolvimento do layout','10.90', 3);

create table orcamentos(
    id_orcamento int not null auto_increment,
    dt_criacao date not null,
    dt_validade date not null,
    dt_entrega date,
    situacao int not null,
    titulo varchar(100) not null,
    descricao text,
    desconto float,
    fk_cliente int not null,
    orcamento tinyint default 0,
    primary key (id_orcamento),
    foreign key (fk_cliente) references clientes (id_cliente)
);

insert into orcamentos (dt_criacao, dt_validade, dt_entrega, situacao, titulo, descricao, desconto, fk_cliente, orcamento)
    values  ('2017-11-17', '2017-11-27', '2017-12-27', 2, 'Meu primeiro Orçamento', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam faucibus id tellus scelerisque pellentesque. Proin consequat lacus eget leo aliquet tincidunt. Sed urna tellus, accumsan et leo quis, luctus lobortis augue. Duis mollis vestibulum nunc ac ultricies. Suspendisse potenti. Donec aliquet pharetra nibh, sed cursus est accumsan ut. Nullam auctor eros vel justo congue, at scelerisque erat vestibulum.', 1.90, 1, 1);

create table itens_orcamentos(
    id_item int not null auto_increment,
    fk_orcamento int not null,
    fk_equipe int not null,
    nome varchar(100) not null,
    esforco int not null,
    preco float not null,
    primary key (id_item),
    foreign key (fk_orcamento) references orcamentos (id_orcamento),
    foreign key (fk_equipe) references equipes (id_equipe)
);

insert into itens_orcamentos (fk_orcamento, fk_equipe, nome, esforco, preco)
    values  (1, 1, 'Treinamento', 5, 19.90),
            (1, 1, 'Desenvolvimento PHP', 60, 9.90);

create table projetos(
    id_projeto int not null auto_increment,
    fk_cliente int not null,
    fk_orcamento int not null,
    titulo varchar(60) not null,
    descricao text not null,
    dt_prevista date not null,
    primary key (id_projeto),
    foreign key (fk_cliente) references clientes (id_cliente),
    foreign key (fk_orcamento) references orcamentos (id_orcamento)
);

insert into projetos (fk_cliente, fk_orcamento, titulo, descricao, dt_prevista)
    values (1,1,'Meu primeiro Projeto', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam faucibus id tellus scelerisque pellentesque. Proin consequat lacus eget leo aliquet tincidunt. Sed urna tellus, accumsan et leo quis, luctus lobortis augue. Duis mollis vestibulum nunc ac ultricies. Suspendisse potenti. Donec aliquet pharetra nibh, sed cursus est accumsan ut. Nullam auctor eros vel justo congue, at scelerisque erat vestibulum.', '2017-11-28');

create table tarefas(
    id_tarefa int not null auto_increment,
    fk_projeto int not null,
    fk_equipe int not null,
    titulo varchar(100) not null,
    situacao int not null default 1,
    primary key (id_tarefa),
    foreign key (fk_projeto) references projetos (id_projeto),
    foreign key (fk_equipe) references equipes (id_equipe)
    );

insert into tarefas (fk_projeto, fk_equipe, titulo)
    values (1,1,'Minha primeira tarefa');

create table movimentacoes_financeiras(
    id_movimentacao int not null auto_increment,
    descricao varchar(100) not null,
    valor decimal(10,2) not null,
    dt_vencimento date not null,
    situacao tinyint default 0,
    primary key (id_movimentacao)
);

insert into movimentacoes_financeiras(descricao, valor, dt_vencimento)
    values  ('Projeto 1', 4990.00, '2017-11-28'),
            ('Aluguel', 4000.00, '2017-11-10'),
            ('Internet', 120.00, '2017-11-10'),
            ('Energia', 180.00, '2017-11-05');